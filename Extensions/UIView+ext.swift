//
//  UIView+ext.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 23/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
import UIKit

extension UIView {


    func viewDropShadow() {
        // self.layer.masksToBounds = true
        self.layer.shadowOpacity = 0.6
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)

        // render and cache the layer
        self.layer.shouldRasterize = true
        // make sure the cache is retina (the default is 1.0)
        self.layer.rasterizationScale = UIScreen.main.scale
    }


    func addDashLineBoarder(dashesColor: UIColor){

        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = dashesColor.cgColor
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = self.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: self.bounds).cgPath
        self.layer.addSublayer(yourViewBorder)
    }


    func addDashBtnBoarder(dashesColor: UIColor,frame: CGRect){

        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = dashesColor.cgColor
        yourViewBorder.lineDashPattern = [2, 2]
        yourViewBorder.frame = frame
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: frame).cgPath
        self.layer.addSublayer(yourViewBorder)
    }

    func addBottomLine(){

        let view = UIView(frame: CGRect(x: 0, y: self.bounds.height, width: UIScreen.main.bounds.width, height: 1))

        view.backgroundColor = UIColor.groupTableViewBackground
        self.addSubview(view)
    }


    func addConstraintsWithFormat(format: String, views: UIView...){

        var viewDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {

            let key = "v\(index)"
            viewDictionary[key] = view

        }


        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewDictionary))

    }
}





extension UIView {



    func pinToSuperView(top: CGFloat? = 0, left: CGFloat? = 0, bottom: CGFloat? = 0, right: CGFloat? = 0){
        guard let superview = self.superview else { return }

        prepareForAutoLayout()

        if let top = top {
            self.topAnchor.constraint(equalTo: superview.topAnchor, constant: top).isActive = true
        }

        if let bottom = bottom {
            self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: bottom).isActive = true
        }

        if let left = left {
            self.leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: left).isActive = true
        }

        if let right = right {
            self.trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: right).isActive = true
        }
    }


    func pinToOtherView(xAxis:NSLayoutXAxisAnchor?,yAxis:NSLayoutYAxisAnchor?,top: CGFloat? = 0, left: CGFloat? = 0, bottom: CGFloat? = 0, right: CGFloat? = 0){


        prepareForAutoLayout()

        if let top = top {
            self.topAnchor.constraint(equalTo: yAxis!, constant: top).isActive = true
        }

        if let bottom = bottom {
            self.bottomAnchor.constraint(equalTo: yAxis!, constant: bottom).isActive = true
        }

        if let left = left {
            self.leadingAnchor.constraint(equalTo: xAxis!, constant: left).isActive = true
        }

        if let right = right {
            self.trailingAnchor.constraint(equalTo: xAxis!, constant: right).isActive = true
        }
    }



    func centerInSuperView(){
        guard let superview = self.superview else { return }

        prepareForAutoLayout()

        self.centerXAnchor.constraint(equalTo: superview.centerXAnchor).isActive = true
        self.centerYAnchor.constraint(equalTo: superview.centerYAnchor).isActive = true
    }

    func constraint(width: CGFloat){
        prepareForAutoLayout()
        self.widthAnchor.constraint(equalToConstant: width).isActive = true
    }

    func constraint(height: CGFloat){
        prepareForAutoLayout()
        self.heightAnchor.constraint(equalToConstant: height).isActive = true
    }

    func makeWidthEqualHeight(){
        prepareForAutoLayout()
        self.widthAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    }

    func constraint(centerX : NSLayoutXAxisAnchor,value:CGFloat){
        prepareForAutoLayout()
        self.centerXAnchor.constraint(equalTo: centerX, constant: value).isActive = true

    }

    func constraint(centerY : NSLayoutYAxisAnchor,value:CGFloat){
        prepareForAutoLayout()
        self.centerYAnchor.constraint(equalTo: centerY, constant: value).isActive = true

    }

    func constraint(greaterThanHeight: CGFloat){
        prepareForAutoLayout()
        self.heightAnchor.constraint(greaterThanOrEqualToConstant: greaterThanHeight).isActive = true
    }

    func prepareForAutoLayout(){
        self.translatesAutoresizingMaskIntoConstraints = false
    }
}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
extension UIView{
   func dropShadow(scale: Bool = true) {

      layer.masksToBounds = false
      layer.shadowColor = UIColor.gray.cgColor
      layer.shadowOpacity = 0.5
      layer.shadowOffset = CGSize(width: -1, height: 1)
      layer.shadowRadius = 1

      layer.shadowPath = UIBezierPath(rect: bounds).cgPath
      layer.shouldRasterize = true
      layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }

    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
      layer.masksToBounds = false
      layer.shadowColor = color.cgColor
      layer.shadowOpacity = opacity
      layer.shadowOffset = offSet
      layer.shadowRadius = radius

      layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
      layer.shouldRasterize = true
      layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }

    func dropShadow4(){
//         dropShadow(color: .black, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
//         dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        dropShadow()
    }
    func dropShadowItem(){
        dropShadow()
        //dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
    }


}
extension UIView {
  func addTopBorderWithColor(color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
    self.layer.addSublayer(border)
  }

  func addRightBorderWithColor(color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
    self.layer.addSublayer(border)
  }

  func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
    self.layer.addSublayer(border)
  }

  func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
    self.layer.addSublayer(border)
  }
}
