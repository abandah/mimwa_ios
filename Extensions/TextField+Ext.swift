//
//  TextField+Ext.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 23/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//
import UIKit

extension UITextField{

    func isValidPhoneNumber() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^(05)(5|0|3|6|4|9|1|8|7)([0-9]{7})$", options: .init())
        return regex.firstMatch(in: self.text!, options: [], range: NSRange(location: 0, length: text!.count)) != nil
    }

    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self.text!, options: [], range: NSRange(location: 0, length: text!.count)) != nil
    }

    func isValidText() -> Bool {
        if text != nil {
            let trimmedString = text!.trimmingCharacters(in: .whitespaces)
            if trimmedString.isEmpty{
                return false
            }else{

                return true
            }
        }else{
            return false
        }
    }

}



extension UITextView{

    func isValidText() -> Bool {
        if text != nil {
            let trimmedString = text!.trimmingCharacters(in: .whitespaces)
            if trimmedString.isEmpty{
                return false
            }else{

                return true
            }
        }else{
            return false
        }
    }
}
