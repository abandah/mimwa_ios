//
//  String+Ext.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 23/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit

extension String {


    func slice(from: String, to: String) -> String? {

        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
    func toDate()  -> Date{
        let newTime = Date(timeIntervalSince1970: parseDuration(self))
        return newTime
    }
    func parseDuration(_ timeString:String) -> TimeInterval {
        guard !timeString.isEmpty else {
            return 0
        }

        var interval:Double = 0

        let parts = timeString.components(separatedBy: ":")
        for (index, part) in parts.reversed().enumerated() {
            interval += (Double(part) ?? 0) * pow(Double(60), Double(index))
        }

        return interval
    }
    func ToInt()-> Int{

        return Int(self) ?? 0
    }
    func ToFloat()-> Float{

        return Float(self) ?? 0
    }

    //    var localize: String {
    //        return NSLocalizedString(self, comment: "")
    //    }

    var html2AttributedString: NSAttributedString? {
        do {
            return try  NSAttributedString(data: Data(utf8),
                                           options: [
                                            .documentType: NSAttributedString.DocumentType.html,
                                            .characterEncoding: String.Encoding.utf8.rawValue
                ], documentAttributes: nil)
        } catch {
            print("error:", error)
            return nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }


    func getCountryPhonceCode (countryCode : String) -> String
    {

        let countryDictionary = ["AF":"93", "AL":"355", "DZ":"213","AS":"1", "AD":"376", "AO":"244", "AI":"1","AG":"1","AR":"54","AM":"374","AW":"297","AU":"61","AT":"43","AZ":"994","BS":"1","BH":"973","BD":"880","BB":"1","BY":"375","BE":"32","BZ":"501","BJ":"229","BM":"1","BT":"975","BA":"387","BW":"267","BR":"55","IO":"246","BG":"359","BF":"226","BI":"257","KH":"855","CM":"237","CA":"1","CV":"238","KY":"345","CF":"236","TD":"235","CL":"56","CN":"86","CX":"61","CO":"57","KM":"269","CG":"242","CK":"682","CR":"506","HR":"385","CU":"53","CY":"537","CZ":"420","DK":"45","DJ":"253","DM":"1","DO":"1","EC":"593","EG":"20","SV":"503","GQ":"240","ER":"291","EE":"372","ET":"251","FO":"298","FJ":"679","FI":"358","FR":"33","GF":"594","PF":"689","GA":"241","GM":"220","GE":"995","DE":"49","GH":"233","GI":"350","GR":"30","GL":"299","GD":"1","GP":"590","GU":"1","GT":"502","GN":"224","GW":"245","GY":"595","HT":"509","HN":"504","HU":"36","IS":"354","IN":"91","ID":"62","IQ":"964","IE":"353","IL":"972","IT":"39","JM":"1","JP":"81","JO":"962","KZ":"77","KE":"254","KI":"686","KW":"965","KG":"996","LV":"371","LB":"961","LS":"266","LR":"231","LI":"423","LT":"370","LU":"352","MG":"261","MW":"265","MY":"60","MV":"960","ML":"223","MT":"356","MH":"692","MQ":"596","MR":"222","MU":"230","YT":"262","MX":"52","MC":"377","MN":"976","ME":"382","MS":"1","MA":"212","MM":"95","NA":"264","NR":"674","NP":"977","NL":"31","AN":"599","NC":"687","NZ":"64","NI":"505","NE":"227","NG":"234","NU":"683","NF":"672","MP":"1","NO":"47","OM":"968","PK":"92","PW":"680","PA":"507","PG":"675","PY":"595","PE":"51","PH":"63","PL":"48","PT":"351","PR":"1","QA":"974","RO":"40","RW":"250","WS":"685","SM":"378","SA":"966","SN":"221","RS":"381","SC":"248","SL":"232","SG":"65","SK":"421","SI":"386","SB":"677","ZA":"27","GS":"500","ES":"34","LK":"94","SD":"249","SR":"597","SZ":"268","SE":"46","CH":"41","TJ":"992","TH":"66","TG":"228","TK":"690","TO":"676","TT":"1","TN":"216","TR":"90","TM":"993","TC":"1","TV":"688","UG":"256","UA":"380","AE":"971","GB":"44","US":"1", "UY":"598","UZ":"998", "VU":"678", "WF":"681","YE":"967","ZM":"260","ZW":"263","BO":"591","BN":"673","CC":"61","CD":"243","CI":"225","FK":"500","GG":"44","VA":"379","HK":"852","IR":"98","IM":"44","JE":"44","KP":"850","KR":"82","LA":"856","LY":"218","MO":"853","MK":"389","FM":"691","MD":"373","MZ":"258","PS":"970","PN":"872","RE":"262","RU":"7","BL":"590","SH":"290","KN":"1","LC":"1","MF":"590","PM":"508","VC":"1","ST":"239","SO":"252","SJ":"47","SY":"963","TW":"886","TZ":"255","TL":"670","VE":"58","VN":"84","VG":"284","VI":"340"]

        if  countryDictionary[countryCode] != nil{
            return "+\(countryDictionary[countryCode]!)"
        }else{
            return ""
        }
    }

    /**
     Pads the left side of a string with the specified string up to the specified length.
     Does not clip the string if too long.

     - parameter padding:   The string to use to create the padding (if needed)
     - parameter length:    Integer target length for entire string
     - returns: The padded string
     */
    func lpad(_ padding: String, length: Int) -> (String) {
        if self.count > length {
            return self
        }
        return "".padding(toLength: length - self.count, withPad:padding, startingAt:0) + self
    }
    /**
     Pads the right side of a string with the specified string up to the specified length.
     Does not clip the string if too long.

     - parameter padding:   The string to use to create the padding (if needed)
     - parameter length:    Integer target length for entire string
     - returns: The padded string
     */
    func rpad(_ padding: String, length: Int) -> (String) {
        if self.count > length { return self }
        return self.padding(toLength: length, withPad:padding, startingAt:0)
    }
    /**
     Returns string with left and right spaces trimmed off.

     - returns: Trimmed String
     */
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    /**
     Shortcut for getting length (since Swift keeps cahnging this).

     - returns: Int length of string
     */
    var length: Int {
        return self.count
    }
    /**
     Returns character at a specific position from a string.

     - parameter index:               The position of the character
     - returns: Character
     */
    subscript (i: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: i)]
    }
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    /**
     Returns substring extracted from a string at start and end location.

     - parameter start:               Where to start (-1 acceptable)
     - parameter end:                 (Optional) Where to end (-1 acceptable) - default to end of string
     - returns: String
     */
    func stringFrom(_ start: Int, to end: Int? = nil) -> String {
        var maximum = self.count

        let i = start < 0 ? self.endIndex : self.startIndex
        let ioffset = min(maximum, max(-1 * maximum, start))
        let startIndex = self.index(i, offsetBy: ioffset)

        maximum -= start

        let j = end! < 0 ? self.endIndex : self.startIndex
        let joffset = min(maximum, max(-1 * maximum, end ?? 0))
        let endIndex = end != nil && end! < self.count ? self.index(j, offsetBy: joffset) : self.endIndex

      //  return self.slice(from: startIndex, to: endIndex)
        let range = startIndex..<endIndex // If you have a range
        //let newStr = = str.substring(with: range) // Swift 3
        let newStr = String(self[range])
        return newStr
    }
    /**
     Returns substring composed of only the allowed characters.

     - parameter allowed:             String list of acceptable characters
     - returns: String
     */
    func onlyCharacters(_ allowed: String) -> String {
        let search = allowed.self
        return self.filter({ search.contains($0) }).reduce("", { $0 + String($1) })
    }
    /**
     Simple pattern matcher. Requires full match (ie, includes ^$ implicitly).

     - parameter pattern:             Regex pattern (includes ^$ implicitly)
     - returns: true if full match found
     */
    func matches(_ pattern: String) -> Bool {
        let test = NSPredicate(format:"SELF MATCHES %@", pattern)
        return test.evaluate(with: self)
    }


    func calculateContentHeight(_ font: UIFont) -> CGFloat{
        let maxLabelSize: CGSize = CGSize(width: CGFloat(200), height: CGFloat(900))
        let expectedLabelSize = self.boundingRect(with: maxLabelSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        print("\(expectedLabelSize)")
        return expectedLabelSize.size.height

    }



    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.height)
    }



    func isValidPhoneNumber() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^(05)(5|0|3|6|4|9|1|8|7)([0-9]{7})$", options: .init())
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    func isFull() -> Bool {
        return ( !self.isEmpty)
    }



    func isValidText(_ text: String?) -> Bool {
        if text != nil {
            let trimmedString = text!.trimmingCharacters(in: .whitespaces)
            if trimmedString.isEmpty{
                return false
            }else{

                return true
            }
        }else{
            return false
        }
    }
}

extension String {
    var bool: Bool? {
        switch self.lowercased() {
        case "true", "t", "yes", "y", "1", "True":
            return true
        case "false", "f", "no", "n", "0", "False":
            return false
        default:
            return nil
        }
    }
}

extension NSMutableAttributedString{



}

