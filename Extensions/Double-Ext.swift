//
//  Double-Ext.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 09/02/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
extension Double {


    // returns the date formatted.
    func toDate() -> Date? {
        let date = Date(timeIntervalSince1970: self)
        //let dateFormatter = DateFormatter()
       // dateFormatter.dateFormat = "dd/MM/yyyy"
        return date
     }

    // returns the date formatted according to the format string provided.
//    func dateFormatted() -> Date{
//         let date = Date(timeIntervalSince1970: self)
//         let dateFormatter = DateFormatter()
//         dateFormatter.dateFormat = format
//         return dateFormatter.string(from: date)
//    }

}
