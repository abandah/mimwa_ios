//
//  UILabel+`linHeight.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 23/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit

extension UILabel {
    func setLineHeight(lineHeight: CGFloat) {
        let text = self.text
        if let text = text {
            let attributeString = NSMutableAttributedString(string: text)
            let style = NSMutableParagraphStyle()
            style.alignment = .center
            style.lineSpacing = lineHeight

            attributeString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSMakeRange(0, (attributeString.length)))


            self.attributedText = attributeString
        }
    }
}
