//
//  Date+Ext.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 23/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation

extension Date {
    var jsonDate: String {
        let ticks = lround(timeIntervalSince1970 * 1000)
        return "/Date(\(ticks))/"
    }

    func dateToString(formatType:String)-> String{

        let formatter = DateFormatter()
        if formatType == "app"{
            formatter.dateFormat = "dd/MM/yyyy"
        }else{
            formatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        }
        //yyyy-MM-dd
        let myString = formatter.string(from: self)

        return myString
    }


    func timeToString()-> String{

        let formatter = DateFormatter()

        formatter.dateFormat = "HH:mm"
        //yyyy-MM-dd
        let myString = formatter.string(from: self)

        return myString
    }

}
