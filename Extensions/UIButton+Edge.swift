//
//  UIButton+Edge.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 23/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit

extension UIButton {

    func centerVertically(padding: CGFloat = 6.0,langage: String) {
        guard
            let imageViewSize = self.imageView?.frame.size,
            let titleLabelSize = self.titleLabel?.frame.size else {
                return
        }

        let totalHeight = imageViewSize.height + titleLabelSize.height + padding

        self.imageEdgeInsets = UIEdgeInsets(
            top: -(totalHeight - imageViewSize.height),
            left: 0.0,
            bottom: 0.0,
            right: langage == "ar" ? titleLabelSize.width : -titleLabelSize.width // -titleLabelSize.width
        )

        self.titleEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: langage == "ar" ? imageViewSize.width : -imageViewSize.width,
            bottom: -(totalHeight - titleLabelSize.height),
            right: 0.0
        )

        self.contentEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: 0.0,
            bottom: titleLabelSize.height,
            right: 0.0
        )
    }

}
