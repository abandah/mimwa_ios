//
//  Uibutton+Extension.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 23/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit

extension UIButton {
    func btnDropShadow() {
        // self.layer.masksToBounds = true
        self.layer.shadowOpacity = 0.6
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)

        // render and cache the layer
        self.layer.shouldRasterize = true
        // make sure the cache is retina (the default is 1.0)
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
extension UIButton {
    func loadingIndicator(_ show: Bool) {
        let tag = 808404
        if show {
            self.isEnabled = false
            self.alpha = 0.5
            let indicator = UIActivityIndicatorView()
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.tag = tag
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            self.isEnabled = true
            self.alpha = 1.0
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
}
