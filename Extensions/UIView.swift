//
//  UIView.swift
//  MOID
//
//  Created by Yasser Ali on 8/21/19.
//  Copyright © 2019 Yasser Ali. All rights reserved.
//

import UIKit
//import MBProgressHUD

extension UIView {
    
    @IBInspectable var circular: Bool {
        get { layer.cornerRadius == CGFloat(bounds.width) }
        set {
            layoutIfNeeded()
            layer.cornerRadius = newValue ? CGFloat(bounds.width/2) : 0
        }
    }
    
    @IBInspectable var cornerRadius: Float {
        get { Float(layer.cornerRadius) }
        set {
            clipsToBounds = true
            layer.cornerRadius = CGFloat(newValue)
            layer.masksToBounds = true
        }
    }
    
    @IBInspectable var borderWidth: Float {
        get { Float(layer.borderWidth) }
        set {
            layer.borderWidth = CGFloat(newValue)
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        get { .white }
        set {
            layer.borderColor = newValue.cgColor
        }
    }
    
    @IBInspectable var dropShadow: Bool {
        get {
            return false
        }
        set {
            layer.shadowColor = UIColor.gray.cgColor
            layer.shadowOpacity = 0.75
            layer.shadowRadius = 2
            layer.shadowOffset = CGSize(width: 1, height: 1)
        }
    }
    
    var width: CGFloat { bounds.width }
    
    var height: CGFloat { bounds.height }

    var size: CGSize { CGSize(width: width, height: height) }
    
    var minY: CGFloat { frame.minY }
    
    var maxY: CGFloat { frame.maxY }
    
    var maxX: CGFloat { frame.maxX }
    
    var minX: CGFloat { frame.minX }
    
    var midY: CGFloat { frame.midY }

    var midX: CGFloat { frame.midX }

    func setHeight(_ height: CGFloat) {
        frame = CGRect(origin: frame.origin, size: CGSize(width: width, height: height))
    }
    
    func setWidth(_ width: CGFloat) {
        frame = CGRect(origin: frame.origin, size: CGSize(width: width, height: height))
    }
}

protocol UIViewLoading {}
extension UIView : UIViewLoading {}

extension UIViewLoading where Self : UIView {
    
    static func loadFromNib() -> Self {
        let nibName = "\(self)".split{$0 == "."}.map(String.init).last!
        let nib = UINib(nibName: nibName, bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as! Self
    }
}
