//
//  SearchTableViewCell.swift
//  Mimwa
//
//  Created by Lenvosoft on 01/10/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var outletProfilePic: UIImageView!
    @IBOutlet weak var outletname: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
