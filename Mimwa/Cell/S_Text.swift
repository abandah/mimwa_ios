//
//  ChatMessageCell.swift
//  GroupedMessagesLBTA
//
//  Created by Brian Voong on 8/25/18.
//  Copyright © 2018 Brian Voong. All rights reserved.
//

import UIKit

class S_Text: ParentTableViewCell {
    
    @IBOutlet weak var messageLabel: UILabel!
    override func startFetching() {
        super.startFetching()
        messageLabel.text = chatMessage.Message
        messageLabel.textColor = _fontColor
    }

}






