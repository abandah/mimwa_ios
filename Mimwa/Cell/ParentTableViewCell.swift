//
//  ParentTableViewCell.swift
//  Mimwa
//
//  Created by Lenvosoft on 13/10/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit

class ParentTableViewCell: UITableViewCell {

    var _backgroundColor: UIColor = .darkGray
    var _fontColor: UIColor = .white
    @IBOutlet weak var bubbleBackgroundView: UIView!
    @IBOutlet weak var timeLable: UILabel!

    var chatMessage: Message! {
        didSet {
            startFetching()


        }
    }


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func startFetching() {
        _backgroundColor = (chatMessage.IsIncoming != 1 ? myColors.subblue : myColors.lightgray)!
        _fontColor = chatMessage.IsIncoming != 1 ? .white : .darkGray
        
        bubbleBackgroundView.backgroundColor = _backgroundColor
        timeLable.text = chatMessage.getTime()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
      //  super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)


    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
