//
//  ChatMessageCell.swift
//  GroupedMessagesLBTA
//
//  Created by Brian Voong on 8/25/18.
//  Copyright © 2018 Brian Voong. All rights reserved.
//

import UIKit

class ChatMessageCell: UITableViewCell {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var bubbleBackgroundView: UIView!
    
    var chatMessage: Message! {
        didSet {
            bubbleBackgroundView.backgroundColor = chatMessage.IsIncoming != 1 ? .white : .darkGray
            messageLabel.textColor = chatMessage.IsIncoming  != 1 ? .black : .white
            
            messageLabel.text = chatMessage.Message
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // backgroundColor = .clear
        // bubbleBackgroundView.backgroundColor = .yellow
        // bubbleBackgroundView.layer.cornerRadius = 12
        //bubbleBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        // messageLabel.translatesAutoresizingMaskIntoConstraints = false
        //  messageLabel.numberOfLines = 0
        
        //addSubview(bubbleBackgroundView)
        //addSubview(messageLabel)
        
        // lets set up some constraints for our label
        _ = [
            messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -32),
            messageLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 250),
            
            bubbleBackgroundView.topAnchor.constraint(equalTo: messageLabel.topAnchor, constant: -16),
            bubbleBackgroundView.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor, constant: -16),
            bubbleBackgroundView.bottomAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 16),
            bubbleBackgroundView.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor, constant: 16),
        ]
        //  NSLayoutConstraint.activate(constraints)
        
//        leadingConstraint = messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32)
//        leadingConstraint.isActive = false
//        //
//        trailingConstraint = messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32)
//        trailingConstraint.isActive = true
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    //    required init?(coder aDecoder: NSCoder) {
    //        fatalError("init(coder:) has not been implemented")
    //    }
    
    
}






