//
//  openChatListener.swift
//  Mimwa
//
//  Created by Lenvosoft on 04/10/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation

protocol OpenChatListener {

    func onHaveNumber(userid : String)
}
