//
//  AppDelegate.swift
//  Mimwa
//
//  Created by Lenvosoft on 29/04/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit
import Firebase
import UserNotificationsUI
import IQKeyboardManagerSwift
import CoreLocation


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    let gcmMessageIDKey = "gcm.message_id"
    var window: UIWindow?

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {

        UserDefaults.standard.set(launchOptions, forKey: "newNotfication")
        AppShared.fromNotification = true
        AppShared.lunchFromNotification = true
        return true
    }


    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       // CLLocationManager().requestAlwaysAuthorization()

        FirebaseApp.configure()
        // IQKeyboardManager.shared.enable = true
        if #available(iOS 10.0, *) {
            // For iOS 10 display naotification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {

            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()

        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {

    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        //print(error)
    }


    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {

        print("func 1")
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)

    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        print("func 2")
        completionHandler(UIBackgroundFetchResult.newData)
    }

}
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {

    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //let userInfo = notification.request.content.userInfo

        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        let data = NotificationBodyJSON.FromJSON(notification: notification)


        let state = UIApplication.shared.applicationState;
        if(state == .inactive)
        {
            completionHandler([[.alert, .sound]])
        }
        else if(state == .active)
        {
            let userInfo = notification.request.content.userInfo

            if(data.isSameSystem()){
                if(AppShared.currentUser == ""){
                    NotificationCenter.default.post(name: Notification.Name("NupdateNotification"), object: nil, userInfo: userInfo)
                    completionHandler([[.alert, .sound]])
                }else{
                    if(data.isSameUserANDSameSYStem(CurrentUser: AppShared.currentUser)){
                        NotificationCenter.default.post(name: Notification.Name("NupdateNotification"), object: nil, userInfo: userInfo)
                    }
                    else{
                        completionHandler([[.alert, .sound]])
                    }
                }






            }else{
                completionHandler([[.alert, .sound]])
            }
        }
        else if(state == .background)
        {
            completionHandler([[.alert, .sound]])
        }

    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print("func 4")
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        NotificationCenter.default.post(name: Notification.Name("updateNotification"), object: nil, userInfo: userInfo)
        completionHandler()
    }
}
extension AppDelegate : MessagingDelegate{

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(String(describing: fcmToken))")

        let dataDict:[String: String] = ["token": fcmToken ]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        print("func 5")
    }
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        
//    }

}
