//
//  AppRefrence.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 26/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation


class AppRefrence: NSObject{

    static let sharedInstance = AppRefrence()
    let userId = 0

}



class AppShared: NSObject{
    static var AuthCode = ""
    static var ClienToken  = ""
    static var currentUser = ""
    static var UserId  = ""
    static var ClientId = 1
    static var SystemID = 1
    static var user : User? = nil
    static var system :System? = nil
    static var fromNotification : Bool? = false
    static var lunchFromNotification : Bool? = false
}

class AppSharedKeys{
    static let AuthCodeKey = "AuthCode"
    static let userIdKey = "userId"
    static let verified = "IsVeryfied"
}

class Constant {
    static let NOInternet = "0"
    static let getAuthCode = "GetAuthCode"
    static let getClientToken = "GetClientToken"
}
