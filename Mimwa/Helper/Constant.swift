//
//  Constant.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 29/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
import UIKit
public struct API {

    static let Post = WSConstants.API_URL+"/API/MobileAppWebAPI/Post"
    static let GetAuthCode = WSConstants.API_URL+"/API/MobileAppWebAPI/GetAuthCode"
    static let GetClientToken = WSConstants.API_URL+"/API/MobileAppWebAPI/GetClientToken"
    static let GetAccessToken = WSConstants.API_URL+"/API/MobileAppWebAPI/GetAccessToken"
    static let UploadFilejson = WSConstants.API_URL+"/en/chat/UploadFilejson"
    static let devUrl = WSConstants.API_URL
    
}

public struct ListConstant{
    static let FIRSTINDEX = 0
    static let MAXBUFFER = 10
}

public struct myColors{
    static let mainblue = UIColor(hex: "#13489Eff")
    static let subblue = UIColor(hex: "#00B6CCff")
    static let lightgray = UIColor(hex: "#9c9c9cff")
    static let darkgray = UIColor(hex: "#727272ff")
    static let lightlightgray = UIColor(hex: "#ebebebff")
}
