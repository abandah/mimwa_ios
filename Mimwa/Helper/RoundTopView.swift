//
//  RoundTopView.swift
//  Mimwa
//
//  Created by Lenvosoft on 01/10/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit

class RoundTopView: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
       // roundCorners(corners: [.topLeft, .topRight,.allCorners], radius: 10.0)
        roundCorners(corners: [.allCorners], radius: 10.0)

    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
