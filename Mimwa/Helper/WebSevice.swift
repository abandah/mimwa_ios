//
//  Auth&AccessToken.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 26/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
import Alamofire


class Call<T : Codable> {

    let showprint :Bool = true;

    var Operation : String = ""
    var Model : String = ""
    var InputObject : Array <[String: Any]>! = nil
    var IdentityObject : [String: AnyObject]! = nil
    var accessToken : String? = nil
    var ExtraHeader : String? = nil
    var IsSecure = false;

    var serviceUrl: String = API.Post


    private var headers : HTTPHeaders = [
      ///  "Authorization":"Basic xxx",
       // "Accept":"application/json",
        "Content-Type": "application/json; charset=utf-8",

       // "__AuthCode" : "",
       // "__ClientToken" : "",
       // "__AccessToken" : ""
]

    func addHeader(key : String , value : String) {
        headers = [key: value]
    }
    func getJson() -> [String: AnyObject] {

        let jsonObject = ["Operation":Operation as Any , "Model": Model as Any , "InputObject": InputObject as Any, "IdentityObject": IdentityObject as Any ] as [String: AnyObject]
        return jsonObject
    }

    func dicToJson(inp : [String : Any]?) -> String {
        if(inp == nil ){
            if(showprint){
            print("{}" )
            }
            return "{}" }
        if(inp!.count <= 0){
            if(showprint){
            print("{}" )
            }
            return "{}"
        }
        if let theJSONData = try?  JSONSerialization.data(
            withJSONObject: inp as Any,
            options: .prettyPrinted
            ),
            let theJSONText = String(data: theJSONData,
                                     encoding: String.Encoding.ascii) {


            print(theJSONText )
            return theJSONText
        }
        if(showprint){
        print("{}" )
        }
        return "{}"

    }
    func arrayToJson(inp :Array<[String : Any]>?) -> String {
        if(inp == nil ){
            if(showprint){
            print("{}" )
            }
            return "{}" }
        if(inp!.count <= 0){
            if(showprint){
            print("{}" )
            }
            return "{}"
        }
        if let theJSONData = try?  JSONSerialization.data(
            withJSONObject: inp as Any,
            options: .prettyPrinted
            ),
            let theJSONText = String(data: theJSONData,
                                     encoding: String.Encoding.utf8) {
            var theJSONTextn = theJSONText.replacingOccurrences(of: "{", with: "")
            theJSONTextn = theJSONTextn.replacingOccurrences(of: "}", with: "")
            theJSONTextn = theJSONTextn.replacingOccurrences(of: "[", with: "{")
            theJSONTextn = theJSONTextn.replacingOccurrences(of: "]", with: "}")

            if(showprint){
            print(theJSONTextn )
            }
            return theJSONTextn
        }
        if(showprint){
        print("{}" )
        }
        return "{}"

    }
    func GetAuthCode() {
        let i:[String: AnyObject] = ["PhoneNumber":AppShared.UserId] as [String: AnyObject]
        Operation = "__AFG_AuthCode"
        Model = "Users"
        //serviceUrl = API.GetAuthCode
        InputObject = Array (arrayLiteral: ["PhoneNumber":AppShared.UserId])
        IdentityObject = i
    }
    func GetClientToken() {
        let i:[String: AnyObject] = ["UserId":AppShared.UserId] as [String: AnyObject]
        Operation = "__AFG_ClientToken"
        Model = "Users"
        //serviceUrl = API.GetAuthCode
        InputObject = nil
        IdentityObject = i
    }
    func GetAccessToken() {
        Operation = "__AFG_AccessToken"
        Model = "Users"
        //serviceUrl = API.GetAuthCode
        InputObject = nil
        IdentityObject = nil
    }
    func getAccessToken() {

    }
    func Run(completionHandler: @escaping (WsResponse<T>?, NSError?) -> Void) {
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: "No internet Connection", message: "check your internet connection and try again.", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { action in
               self.Run(completionHandler: completionHandler)
            }))

            var rootViewController = UIApplication.shared.keyWindow?.rootViewController
            if let navigationController = rootViewController as? UINavigationController {
                rootViewController = navigationController.viewControllers.first
            }
            if let tabBarController = rootViewController as? UITabBarController {
                rootViewController = tabBarController.selectedViewController
            }
            //...
            rootViewController?.present(alert, animated: true)
            return
        }
        if(IsSecure && (accessToken == nil || accessToken!.isEmpty))
        {
            let a = AccessCode()
            a.Go(){(Result) in
                if(Result != nil){
                    self.accessToken = Result
                    self.Run(completionHandler: completionHandler)
                }else{
                    a.Go(){(Result) in
                        if(Result != nil){
                            self.accessToken = Result
                            self.Run(completionHandler: completionHandler)
                        }else{
                            a.Go(){(Result) in
                                if(Result != nil){
                                    self.accessToken = Result
                                    self.Run(completionHandler: completionHandler)
                                }else{
                                    
                                     print("3_error in __AccessToken")
                                }

                            }
                             print("2_error in __AccessToken")
                        }

                    }
                     print("1_error in __AccessToken")
                }

            }
            return
        }
        if(showprint){
        print(serviceUrl)
        }


        if !Connectivity.isConnectedToInternet(){
            let message = "No internet connection, please make sure you are connected to the internet"
            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
            completionHandler( nil, errorStr)
        }


        //        if let authToken = UserDefaults.standard.value(forKey: AppSharedKeys.AuthCodeKey) as? String {
        //            headers = ["__AuthCode": authToken
        //            ]
        //        }
        if (!AppShared.AuthCode.isEmpty){
            headers.updateValue(AppShared.AuthCode, forKey: "__AuthCode")
        }
        if (!AppShared.ClienToken.isEmpty) {
            headers.updateValue(AppShared.ClienToken, forKey: "__ClientToken")
        }

        if  let authToken : String   = accessToken{
            if(!authToken.isEmpty){
                headers.updateValue(authToken, forKey: "__AccessToken")
            }
        }



        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            serviceUrl: .pinCertificates(
                certificates: ServerTrustPolicy.certificates(),
                validateCertificateChain: true,
                validateHost: true
            ),
            "insecure.expired-apis.com": .disableEvaluation
        ]

        var manager = SessionManager(
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )

        manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10
        Alamofire.upload(

            multipartFormData: { multipartFormData in
                if(self.showprint){
                print("Headers")
                }
                for head in self.headers{
                    print(head.key + " : " + head.value )
                }
                if(self.showprint){
                print("Reques")
                }

                multipartFormData.append((self.Operation).data(using: .utf8)!, withName: "Operation")
                if(self.showprint){
                print("Operation : "+self.Operation )
                }
                multipartFormData.append((self.Model).data(using: .utf8)!, withName: "Model")
                if(self.showprint){
                    print("Model : "+self.Model )
                }
                if(self.showprint){
                print("InputObject : ")
                }
                multipartFormData.append((self.arrayToJson(inp: self.InputObject)).data(using: .utf8)!, withName: "InputObject")
                if(self.showprint){
                print("IdentityObject : ")
                }
                multipartFormData.append((self.dicToJson(inp: self.IdentityObject)).data(using: .utf8)!, withName: "IdentityObject")

        },
            to: serviceUrl, method: .post,headers: headers,
            encodingCompletion: { encodingResult in
                //  print(self.getJson())

                switch encodingResult {


                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if(self.showprint){
                        print(response.result)
                        }

                        switch response.result{
                        case .success( let jsonValue):

                            do{
                                if(self.showprint){
                                print(jsonValue)
                                }

                                if let data = jsonValue as? [String: AnyObject]{
                                    if let error = data["error"] as? Bool{
                                        if error{
                                            if let message = data["message"] as? String{
                                                let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                                                completionHandler( nil, errorStr)
                                            }
                                        }else{

                                            let obj = try JSONDecoder().decode(WsResponse<T>.self, from: response.data!)
                                            if(self.showprint){
                                            print(obj)
                                            }
                                            completionHandler(obj,nil)

                                        }

                                    }else{
                                        let obj = try JSONDecoder().decode(WsResponse<T>.self, from: response.data!)
                                        if(self.showprint){
                                        print(obj)
                                        }
                                        completionHandler(obj,nil)
                                    }
                                }else{
                                    let obj = try JSONDecoder().decode(WsResponse<T>.self, from: response.data!)
                                    if(self.showprint){
                                    print(obj)
                                    }
                                    completionHandler(obj,nil)
                                }

                            }catch let DecodingError.dataCorrupted(context) {
                                if(self.showprint){
                                print(context)
                                }
                            } catch let DecodingError.keyNotFound(key, context) {
                                if(self.showprint){
                                    print("Key '\(key)' not found:", context.debugDescription)
                                print("codingPath:", context.codingPath)
                                }
                            } catch let DecodingError.valueNotFound(value, context) {
                                if(self.showprint){
                                    print("Value '\(value)' not found:", context.debugDescription)
                                print("codingPath:", context.codingPath)
                                }
                            } catch let DecodingError.typeMismatch(type, context)  {
                                if(self.showprint){
                                print("Type '\(type)' mismatch:", context.debugDescription)
                                print("codingPath:", context.codingPath)
                                }
                            } catch {
                                if(self.showprint){
                                print("error: ", error)
                                }
                            }

                        case .failure(let error):
                            if(self.showprint){
                            print(error.localizedDescription)
                            }
                            let message : String
                            if let httpStatusCode = response.response?.statusCode {
                                switch(httpStatusCode) {
                                case 400:
                                    message = "Something went wrong, please try again"
                                    let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                                    completionHandler( nil, errorStr)
                                case 401:
                                    message = "please login in order to continue"
                                    let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                                    completionHandler( nil, errorStr)
                                default:
                                    message = "Something went wrong, please try again"
                                    let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                                    completionHandler( nil, errorStr)
                                }
                            } else {
                                message = "Something went wrong, please try again"
                                let error = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                                completionHandler(nil, error)
                            }

                        }

                    }
                case .failure(let encodingError):
                    if(self.showprint){
                    print(encodingError)
                    }
                }
        }
        )
    }
    func uploadImageServer<T: Decodable> (imagesData : [UIImage]?,completionHandler: @escaping (T?, NSError?) -> Void){

        serviceUrl = API.UploadFilejson
        if(self.showprint){
        print(serviceUrl)
        }

        if !Connectivity.isConnectedToInternet(){
            let message = "No internet connection, please make sure you are connected to the internet"
            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
            completionHandler( nil, errorStr)
        }

        let _: [String: ServerTrustPolicy] = [
            serviceUrl: .pinCertificates(
                certificates: ServerTrustPolicy.certificates(),
                validateCertificateChain: true,
                validateHost: true
            ),
            "insecure.expired-apis.com": .disableEvaluation
        ]


        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 60



        Alamofire.upload(

            multipartFormData: { multipartFormData in


                multipartFormData.append((AppShared.UserId).data(using: .utf8)!, withName: "UserId")
                multipartFormData.append(("2").data(using: .utf8)!, withName: "UploadType")



                if(imagesData != nil && imagesData!.count > 0){
                    var i = 0
                    for imageData in imagesData! {
                        multipartFormData.append(imageData.jpegData(compressionQuality: 1)!, withName: "\(i)", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")

                        i += 1
                    }
                }
        },
            to: serviceUrl, method: .post,headers: headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {


                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if(self.showprint){
                        print(response.result)
                        }

                        switch response.result{
                        case .success( let jsonValue):

                            do{
                                if(self.showprint){
                                print(jsonValue)
                                }

                                if let data = jsonValue as? [String: AnyObject]{
                                    if let error = data["error"] as? Bool{
                                        if error{
                                            if let message = data["message"] as? String{
                                                let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                                                completionHandler( nil, errorStr)
                                            }
                                        }else{

                                            let obj = try JSONDecoder().decode(T.self, from: response.data!)
                                            if(self.showprint){
                                            print(obj)
                                            }
                                            completionHandler(obj,nil)

                                        }

                                    }else{
                                        let obj = try JSONDecoder().decode(T.self, from: response.data!)
                                        if(self.showprint){
                                        print(obj)
                                        }
                                        completionHandler(obj,nil)
                                    }
                                }else{
                                    let obj = try JSONDecoder().decode(T.self, from: response.data!)
                                    if(self.showprint){
                                    print(obj)
                                    }
                                    completionHandler(obj,nil)
                                }

                            }catch let DecodingError.dataCorrupted(context) {
                                if(self.showprint){
                                print(context)
                                }
                            } catch let DecodingError.keyNotFound(key, context) {
                                if(self.showprint){
                                print("Key '\(key)' not found:", context.debugDescription)
                                print("codingPath:", context.codingPath)
                                }
                            } catch let DecodingError.valueNotFound(value, context) {
                                if(self.showprint){
                                print("Value '\(value)' not found:", context.debugDescription)
                                print("codingPath:", context.codingPath)
                                }
                            } catch let DecodingError.typeMismatch(type, context)  {
                                if(self.showprint){
                                print("Type '\(type)' mismatch:", context.debugDescription)
                                print("codingPath:", context.codingPath)
                                }
                            } catch {
                                if(self.showprint){
                                print("error: ", error)
                                }
                            }

                        case .failure(let error):
                            if(self.showprint){
                            print(error.localizedDescription)
                            }
                            let message : String
                            if let httpStatusCode = response.response?.statusCode {
                                switch(httpStatusCode) {
                                case 400:
                                    message = "Something went wrong, please try again"
                                    let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                                    completionHandler( nil, errorStr)
                                case 401:
                                    message = "please login in order to continue"
                                    let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                                    completionHandler( nil, errorStr)
                                default:
                                    message = "Something went wrong, please try again"
                                    let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                                    completionHandler( nil, errorStr)
                                }
                            } else {
                                message = "Something went wrong, please try again"
                                let error = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                                completionHandler(nil, error)
                            }

                        }

                    }
                case .failure(let encodingError):
                    if(self.showprint){
                    print(encodingError)
                    }
                }
        }
        )
    }




}
class AccessCode : Decodable{

     func Go(completionHandler2: @escaping (String?) -> Void) {
          let c : Call = Call<ObjectNull>()
                 c.GetAccessToken()
                 c.Run() {(result:WsResponse<ObjectNull>?, apiError) in
                     if apiError == nil{
                        if let auth = result?.Value?.OutPut![0].__AccessToken{
                            completionHandler2(auth)
                         }else{
                            completionHandler2(nil)
                         }
                         //                    if let res = result as WsResponse<T>{
                         //
                         //                    }else{
                         //
                         //                    }

                     }else{
                          completionHandler2(nil)
                     }
                 }
    }


}

