//
//  NetworkManager.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 23/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
import Alamofire

class GenericConnector: NSObject {
    
    public enum CommunicatorHTTPMethod: Int {
        case get     = 0
        case post    = 1
        case delete = 2
        case Put = 3
        case patch = 4
    }
    
    public struct Client {
        static var Token: String?
        static var accessToken: String?
    }
    
    static func baseImageUrl() -> String?{
        guard let baseUrl = UserDefaults.standard.value(forKey: "apiUrl") as? String else{return nil}
        let newBaseUrl = baseUrl.replacingOccurrences(of: "api.", with: "")
        return newBaseUrl
    }
    
    static var headers : HTTPHeaders = [
        "Accept":"application/json",
        "Content-Type": "application/x-www-form-urlencoded"
    ]
    
    
    static var AccessTokenHeaders : HTTPHeaders = [
        "Accept":"application/json",
        "Content-Type": "application/x-www-form-urlencoded"//"application/json"
    ]
}
extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}


class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}



