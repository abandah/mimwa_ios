//
//  KigFisher.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 02/03/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
import Kingfisher
extension KingfisherWrapper where Base: KFCrossPlatformImageView {
    public func setStringImage(
           with resource: String?,
           placeholder: Placeholder? = nil,
           options: KingfisherOptionsInfo? = nil,
           progressBlock: DownloadProgressBlock? = nil,
           completionHandler: ((Result<RetrieveImageResult, KingfisherError>) -> Void)? = nil) -> DownloadTask?
       {
           return setImage(
            with: URL(string: resource!)?.convertToSource(),
               placeholder: placeholder,
               options: options,
               progressBlock: progressBlock,
               completionHandler: completionHandler)
       }


}

