//
//  LocationService.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 03/03/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class LocationService :UIViewController ,CLLocationManagerDelegate
{

    var locationManager: CLLocationManager = CLLocationManager()

    static var  latlng: LATLNG = LATLNG()

    func UpdateLocation(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.activityType = .automotiveNavigation
        locationManager.distanceFilter = 10.0  // Movement threshold for new events
        locationManager.allowsBackgroundLocationUpdates = true // allow in background
        self.locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            self.locationManager.startUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation

        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")

        LocationService.self.latlng.latitude = userLocation.coordinate.latitude
        LocationService.self.latlng.longitude = userLocation.coordinate.longitude

        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks![0]
                print(placemark.locality!)
                print(placemark.administrativeArea!)
                print(placemark.country!)

                LocationService.self.latlng.locationString = "\(placemark.locality!), \(placemark.administrativeArea!), \(placemark.country!)"
            }
        }

    }


}

struct LATLNG {
    var  longitude,latitude : Double!
    var locationString : String!
}
