//
//  HalfSizePresentationController.swift
//  Mimwa
//
//  Created by Lenvosoft on 01/10/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//
import UIKit
import Foundation

class HalfSizePresentationController : UIPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        get {
            guard let theView = containerView else {
                return CGRect.zero
            }

            return CGRect(x: 0, y: theView.bounds.height/2, width: theView.bounds.width, height: theView.bounds.height/2)
        }
    }
}
