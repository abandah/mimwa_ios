//
//  Auth&AccessToken.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 26/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
import Alamofire


class GenericAccessToken: NSObject {


    public enum CommunicatorHTTPMethod: Int {
        case get     = 0
        case post    = 1
        case delete = 2
        case Put = 3
        case patch = 4
    }


    public struct Client {
        static var Token: String?//""
    }



    class func fetchToken<T: Decodable> (accessToken: String?,serviceName: String, jsonEncode: ParameterEncoding,httpMethod: HTTPMethod,json: [String: AnyObject]?,completionHandler: @escaping (T?, NSError?) -> Void){


        var serviceUrl: String = ""
        //var loginHeader = false
        serviceUrl += serviceName
        print(serviceUrl)


        if !Connectivity.isConnectedToInternet(){
            let message = "No internet connection, please make sure you are connected to the internet"
            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
            completionHandler( nil, errorStr)
        }



        var headers : HTTPHeaders = [
            "Authorization":"Basic xxx"
        ]

        if let authToken = UserDefaults.standard.value(forKey: AppSharedKeys.AuthCodeKey) as? String {
            headers = ["__AuthCode": authToken
            ]
        }


        if Client.Token != nil{
            headers.updateValue(Client.Token!, forKey: "__ClientToken")
        }


        if  accessToken != nil{
            headers.updateValue(accessToken!, forKey: "__AccessToken")
        }



        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            serviceUrl: .pinCertificates(
                certificates: ServerTrustPolicy.certificates(),
                validateCertificateChain: true,
                validateHost: true
            ),
            "insecure.expired-apis.com": .disableEvaluation
        ]

        var manager = SessionManager(
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )

        manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10

        var encodingType: ParameterEncoding{
            if httpMethod == .post || httpMethod == .patch{
                return URLEncoding.default
            }else{
                return URLEncoding.default
            }
        }

        manager.request(serviceUrl, method: httpMethod, parameters: json, encoding: encodingType, headers: headers)

            .responseJSON { response in

                switch(response.result) {
                case .success( let jsonValue):
                    // Yeah! Hand response
                    do{
                        print(jsonValue)

                        if let data = jsonValue as? [String: AnyObject]{
                            if let error = data["error"] as? Bool{
                                if error{
                                    if let message = data["message"] as? String{
                                        let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                                        completionHandler( nil, errorStr)
                                    }
                                }else{

                                    let obj = try JSONDecoder().decode(T.self, from: response.data!)
                                    print(obj)
                                    completionHandler(obj,nil)

                                }

                            }else{
                                let obj = try JSONDecoder().decode(T.self, from: response.data!)
                                print(obj)
                                completionHandler(obj,nil)
                            }
                        }else{
                            let obj = try JSONDecoder().decode(T.self, from: response.data!)
                            print(obj)
                            completionHandler(obj,nil)
                        }

                    } catch let jsonError{

                        print(jsonError.localizedDescription)
                    }

                case .failure(let error):
                    print(error.localizedDescription)
                    let message : String
                    if let httpStatusCode = response.response?.statusCode {
                        switch(httpStatusCode) {
                        case 400:
                            message = "Something went wrong, please try again"
                            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                            completionHandler( nil, errorStr)
                        case 401:
                            message = "please login in order to continue"
                            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                            completionHandler( nil, errorStr)
                        default:
                            message = "Something went wrong, please try again"
                            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                            completionHandler( nil, errorStr)
                        }
                    } else {
                        message = "Something went wrong, please try again"
                        let error = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                        completionHandler(nil, error)
                    }
                }
        }
    }



    class func fetchStringToken<T: Decodable> (accessToken: String?,serviceName: String, jsonEncode: ParameterEncoding,httpMethod: HTTPMethod,json: [String: AnyObject]?,completionHandler: @escaping (T?, NSError?) -> Void){


        var serviceUrl: String = ""
        //var loginHeader = false
        serviceUrl += serviceName
        print(serviceUrl)


        if !Connectivity.isConnectedToInternet(){
            let message = "No internet connection, please make sure you are connected to the internet"
            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
            completionHandler( nil, errorStr)
        }



        var headers : HTTPHeaders = [
            "Authorization":"Basic xxx"
        ]

        if let authToken = UserDefaults.standard.value(forKey: AppSharedKeys.AuthCodeKey) as? String {
            headers = ["__AuthCode": authToken
            ]
        }


        if Client.Token != nil{
            headers.updateValue(Client.Token!, forKey: "__ClientToken")
        }


        if  accessToken != nil{
            headers.updateValue(accessToken!, forKey: "__AccessToken")
        }



        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            serviceUrl: .pinCertificates(
                certificates: ServerTrustPolicy.certificates(),
                validateCertificateChain: true,
                validateHost: true
            ),
            "insecure.expired-apis.com": .disableEvaluation
        ]

        var manager = SessionManager(
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )

        manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10

        var encodingType: ParameterEncoding{
            if httpMethod == .post || httpMethod == .patch{
                return URLEncoding.default
            }else{
                return URLEncoding.default
            }
        }

        manager.request(serviceUrl, method: httpMethod, parameters: json, encoding: encodingType, headers: headers)

            .responseString { response in

                switch(response.result) {
                case .success( let jsonValue):
                    // Yeah! Hand response
                    do{
                        print(jsonValue)

//                        if let data = jsonValue as? [String: AnyObject]{
//                            if let error = data["error"] as? Bool{
//                                if error{
//                                    if let message = data["message"] as? String{
//                                        let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
//                                        completionHandler( nil, errorStr)
//                                    }
//                                }else{
//
//                                    let obj = try JSONDecoder().decode(T.self, from: response.data!)
//                                    print(obj)
//                                    completionHandler(obj,nil)
//
//                                }
//
//                            }else{
//                                let obj = try JSONDecoder().decode(T.self, from: response.data!)
//                                print(obj)
//                                completionHandler(obj,nil)
//                            }
//                        }else{
                            let obj = try JSONDecoder().decode(T.self, from: response.data!)
                            print(obj)
                            completionHandler(obj,nil)
                       // }

                    } catch let jsonError{

                        print(jsonError.localizedDescription)
                    }

                case .failure(let error):
                    print(error.localizedDescription)
                    let message : String
                    if let httpStatusCode = response.response?.statusCode {
                        switch(httpStatusCode) {
                        case 400:
                            message = "Something went wrong, please try again"
                            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                            completionHandler( nil, errorStr)
                        case 401:
                            message = "please login in order to continue"
                            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                            completionHandler( nil, errorStr)
                        default:
                            message = "Something went wrong, please try again"
                            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                            completionHandler( nil, errorStr)
                        }
                    } else {
                        message = "Something went wrong, please try again"
                        let error = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                        completionHandler(nil, error)
                    }
                }
        }
    }




    class func GetAccesToken (serviceName: String, jsonEncode: ParameterEncoding,httpMethod: CommunicatorHTTPMethod,json: [String: AnyObject]?,completionHandler: @escaping (String?, NSError?) -> Void){

        var serviceUrl: String?

        serviceUrl = ""

        print(serviceUrl!)

        if !Connectivity.isConnectedToInternet(){
            let message = "No internet connection, please make sure you are connected to the internet"
            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
            completionHandler(nil, errorStr)
        }



        var headers : HTTPHeaders = [
            "Authorization":"Basic xxx"
        ]

        if let authToken = UserDefaults.standard.value(forKey: "authCode") as? String {
            headers = ["__AuthCode": authToken
            ]
        }

        if Client.Token != nil{
            headers.updateValue(Client.Token!, forKey: "__ClientToken")
        }

        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            serviceUrl!: .pinCertificates(
                certificates: ServerTrustPolicy.certificates(),
                validateCertificateChain: true,
                validateHost: true
            ),
            "insecure.expired-apis.com": .disableEvaluation
        ]

        var manager = SessionManager(
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )

        manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 10

        let accessTokenUrl = "http://app.offerswiper.com/AdminSide/WEBSRV/MobileApp.asmx/GetAccessToken"

        guard let userId = UserDefaults.standard.value(forKey: "UserId") as? Int else{return}
        let jsonToken = ["UserId": userId]

        manager.request(accessTokenUrl, method: .get, parameters: jsonToken, encoding: URLEncoding.default, headers: headers)

            .responseJSON { response in

                switch(response.result) {
                case .success( let jsonValue):
                    // Yeah! Hand response
                    print(jsonValue)

                    guard let jsonData = jsonValue as? [String: AnyObject] else{return}
                    if let accesToken = jsonData["AuthCode"] as? String{
                        completionHandler(accesToken, nil)
                    }
                    // completionHandler(nil, jsonData, nil)


                case .failure(let error):
                    print(error.localizedDescription)
                    let message : String
                    if let httpStatusCode = response.response?.statusCode {
                        switch(httpStatusCode) {
                        case 400:
                            message = "Something went wrong, please try again"
                            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                            completionHandler(nil, errorStr)
                        case 401:

                            message = ""
                            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                            completionHandler(nil, errorStr)
                        default:
                            message = "Something went wrong, please try again"
                            let errorStr = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                            completionHandler(nil,errorStr)
                        }
                    } else {
                        message = "Something went wrong, please try again"
                        let error = NSError(domain: "ListClass", code: 1, userInfo: [NSLocalizedDescriptionKey:  message])
                        completionHandler(nil, error)
                    }
                }
        }
    }
}
