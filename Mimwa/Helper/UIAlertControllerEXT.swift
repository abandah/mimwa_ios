//
//  UIAlertControllerEXT.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 08/03/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    static func actionSheetWithItems(items : [(title : String, value : Int)], currentSelection : Int? = nil, action : @escaping (Int) -> Void) -> UIAlertController {
        let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for (var title, value) in items {
            if let selection = currentSelection, value == selection {
                // Note that checkmark and space have a neutral text flow direction so this is correct for RTL
                title = "✔︎ " + title
            }
            controller.addAction(
                UIAlertAction(title: title, style: .default) {_ in
                    action(value)
                }
            )
        }
        return controller
    }
}
