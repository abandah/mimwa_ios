//
//  WSConstants.swift
//  Mimwa
//
//  Created by Lenvosoft on 06/05/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
class WSConstants {

    //static let thumbanail :String = API_URL+"/DCC/resizeimage.aspx?width=150&height=150&crop=true&image=";
    static let thumbanailprofile :String = API_URL+"/DCC/resizeimage.aspx?width=80&height=80&crop=true&image=";
    static let thumbanailuplodedpic :String = API_URL+"/DCC/resizeimage.aspx?width=500&height=400&crop=true&image=";
    static let viewuplodedpic :String = API_URL+"/DCC/resizeimage.aspx?width=500&height=400&crop=true&image=";

    static let thumbanailmainprofile : String = API_URL+"/DCC/resizeimage.aspx?width=80&height=80&crop=true&image=";
    static let staticMap : String = thumbanailuplodedpic+"~/Views/Custom/Chat/Assets/img/StaticMap_Default.jpg"
    static let QR_Default : String = thumbanailuplodedpic+"~/Views/Custom/Chat/Assets/img/QR_Default.jpg"
    static let NFC_Default : String = thumbanailuplodedpic+"~/Views/Custom/Chat/Assets/img/NFC_Default.jpg"
    static let API_URL :String = "https://chat.meemwaw.com:7001";
    static let siteurl :String = "https://chat.meemwaw.com:7001";
    static let UserImgePath :String = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT_ANU4t8VnR3VvSwOgzaVgW9C8R9ushLjQoi3ffrrt_ulmdKtn&usqp=CAU";
    static let Link :String = "https://meet.jit.si";
    //static let Link :String = "https://vctester.com";
 }
