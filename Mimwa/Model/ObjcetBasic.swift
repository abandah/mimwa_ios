//
//  ObjcetBasic.swift
//  Mimwa
//
//  Created by Lenvosoft on 05/05/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
struct ObjcetBasic: Codable {

    let PhoneNumber : String?
    let __AuthCode : String?
    let __ClientToken : String?
    let FilePath : String?
    let Status: Int?
    enum CodingKeys: String, CodingKey {
        case PhoneNumber = "PhoneNumber"
        case __AuthCode = "__AuthCode"
        case __ClientToken = "__ClientToken"
        case FilePath = "FilePath"
         case Status = "Status"

    }
}


