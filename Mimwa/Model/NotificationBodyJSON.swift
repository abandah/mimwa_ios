//
//  NotificationBodyJSON.swift
//  Mimwa
//
//  Created by Lenvosoft on 07/10/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
import Firebase
import UserNotificationsUI

class NotificationBodyJSON : Codable{
    var Category :String //Message
    var mType : String
    var Content : String
    var SenderId :String
    var SystemId : String
    var ClientId : String
    var MessageId : String

    enum CodingKeys: String, CodingKey {
        case Category = "Category"
        case mType = "Type"
        case Content = "Content"
        case SenderId = "SenderId"
        case SystemId = "SystemId"
        case ClientId = "ClientId"
        case MessageId = "MessageId"
    }

    static func FromJSON(notification : UNNotification) -> NotificationBodyJSON {
        let userInfo = notification.request.content.userInfo

        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)

        //print(userInfo[""])
        // Print message ID.
        let paramKey = userInfo["ParamKey"] as! String
        //let data = NotificationBodyJSON.FromJSON(json: paramKey as! String)
        //print("Message ID: \(data)")


        return try! JSONDecoder().decode(NotificationBodyJSON.self, from: paramKey.data(using: .utf8)!)
    }
    static func FromJSON(dic : [AnyHashable : Any]) -> NotificationBodyJSON? {
        guard let paramKey = dic["ParamKey"] as? String else {

            guard (dic["UIApplicationLaunchOptionsRemoteNotificationKey"] as? [AnyHashable : Any])!["ParamKey"] != nil else {
                return nil
            }
            let param : String = (dic["UIApplicationLaunchOptionsRemoteNotificationKey"] as? [AnyHashable : Any])!["ParamKey"] as! String 
            return try! JSONDecoder().decode(NotificationBodyJSON.self, from: param.data(using: .utf8)!)
        }
        //let paramKey = dic["ParamKey"] as! String
        return try! JSONDecoder().decode(NotificationBodyJSON.self, from: paramKey.data(using: .utf8)!)

      //  return nil

    }
    static func FromJSON(sender : NSNotification) -> NotificationBodyJSON {

        let paramKey = sender.userInfo!["ParamKey"] as! String


        return try! JSONDecoder().decode(NotificationBodyJSON.self, from: paramKey.data(using: .utf8)!)
    }
    func isSameSystem() -> Bool {
        return AppShared.SystemID == Int(SystemId) && AppShared.ClientId == Int(ClientId)
    }

    func isSameUserANDSameSYStem(CurrentUser : String) -> Bool {
         return isSameSystem() && CurrentUser == SenderId
    }


}
