//
//  Message.swift
//  Mimwa
//
//  Created by Lenvosoft on 08/10/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
class Message: Codable {
    var FK_MessageTypeID: Int! ;//1 text 2 image 5 location
    var Rownum:Int!
    var SystemId:Int!
    var MessageID:Int!
    var MessageId : Int!
    var SenderID : String!
    var SenderClientId: Int!
    var SenderDisplayName:String!
    var SenderImage : String!
    var Message:String!
    var RecepientID:String!
    var RecepientClientId:Int!
    var RecepientDisplayName:String!
    var RecepientImage:String!
    var IsSeen:Bool!
    var IsRecieved:Bool!
    var IsBlocked:Bool!
    var IsIncoming : Int!
    var MessageDateTime : String!
//    {
//        get{
//            AppShared.UserId == SenderID
//        }
//    }

    func get_MessagePic_thumbanail() -> String{
        if(FK_MessageTypeID == 2 ){
            return WSConstants.thumbanailuplodedpic + Message
        }
        else if (FK_MessageTypeID == 5){
            return WSConstants.staticMap
        }
        else if (FK_MessageTypeID == 10){
            return WSConstants.QR_Default
        }
        else if (FK_MessageTypeID == 11){
            return WSConstants.NFC_Default
        }
        else{
            return WSConstants.QR_Default

        }

    }

    func get_MessagePic_Full() -> String{
            if(FK_MessageTypeID == 2){
                return WSConstants.viewuplodedpic + Message//.replacingOccurrences(of: "~", with: WSConstants.siteurl)
            }else{
                return ""
            }

    }
   // let MessageDate:Date
   // let id:String
   // let text:String
    //let createdAt:Date

    init(id : Int) {
        FK_MessageTypeID = id
    }

    func getTime() -> String {
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        // date format getting from server
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSSS'Z'"

        var date = dateFormatter.date(from: MessageDateTime!)

        if(date == nil){
            dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSSS"

             date = dateFormatter.date(from: MessageDateTime!)
        }
        if(date == nil){
            dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss"

             date = dateFormatter.date(from: MessageDateTime!)
        }
        //date format you want
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date ?? Date())
        return dateString

    }
    func getDate() -> String {
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        // date format getting from server
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSSS'Z'"

        var date = dateFormatter.date(from: MessageDateTime!)
        if(date == nil){
            dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSSS"
             date = dateFormatter.date(from: MessageDateTime!)
        }
        if(date == nil){
            dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss"
             date = dateFormatter.date(from: MessageDateTime!)
        }
        //date format you want
        dateFormatter.dateFormat = "dd/MM/YYYY"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date ?? Date())
        return dateString

    }
    func setTodatDate() {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
        MessageDateTime = dateFormatter.string(from: date)
    }
}
