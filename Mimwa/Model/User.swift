//
//  User.swift
//  Mimwa
//
//  Created by Lenvosoft on 07/05/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
struct User : Codable{
    
    let online :Bool?
    let sellected :Bool? = false
    let FK_UserId :String?
    let FK_SystemId :Int?
    var UserDisplayName :String!
    var UserImageFilePath :String?
    let LastMessage  :String?
    let LastMessageDateTime :String?
    let  NewMessages  :Int?
    let LastSeen :String?
    
    let UserId :  String?
    let PhoneNumber :  String?
    let Email :  String?
    let IsPhoneVerified : Bool!
    let LastSeenDateTime :  String?
    let id : String?
    let text : String?

//    func getName() -> String {
//        if(!(UserDisplayName?.trim().isEmpty)!){
//            return UserDisplayName;
//
//        }else if(!(text?.trim().isEmpty)!){
//            return UserDisplayName;
//
//        }else{
//            return PhoneNumber ?? ""
//        }
//    }
    
    enum CodingKeys: String, CodingKey {
        case online = "online"
        case sellected = "sellected"
        case FK_UserId = "FK_UserId"
        case FK_SystemId = "FK_SystemId"
        case UserDisplayName = "UserDisplayName"
        case UserImageFilePath = "UserImageFilePath"
        case LastMessage = "LastMessage"
        case LastMessageDateTime = "LastMessageDateTime"
        case NewMessages = "NewMessages"
        case LastSeen = "LastSeen"
        
        case UserId = "UserId"
        case PhoneNumber = "PhoneNumber"
        case Email = "Email"
        case IsPhoneVerified = "IsPhoneVerified"
        case LastSeenDateTime = "LastSeenDateTime"
        case id = "id"
        case text = "text"
    }
    mutating func setUserDisplayName(userDisplayName : String){
        self.UserDisplayName = userDisplayName
    }
    
    func  getLastSeen() -> String {
        return LastSeen!;
    }
    func getnewMeesage() -> Int {
        if(NewMessages == nil)
        {
            return 0
        }else{
            return NewMessages!
        }
    }
    func  getId() -> String{
        if(FK_UserId == nil || FK_UserId == ""){
            if(id == nil || id == ""){
                if(UserId == nil || UserId == ""){
                    return PhoneNumber!
                }
                else{
                    return UserId!
                }
            }
            else{
                return id!
            }
            
        }else{
            return FK_UserId!
        }
    }
    
    func  getName() -> String {
        if(UserDisplayName == nil || UserDisplayName == ""){
            if(text == nil || text == ""){
                if(FK_UserId == nil || FK_UserId == ""){

                    return PhoneNumber!;
                }
                return FK_UserId!;
            }
           return text!
        }
        return UserDisplayName!
    }
    
    mutating func  getAvatar() -> String {
        return getUserImageFilePath();
    }
    
    //    func getdate() -> Date {
    //        let dateFormatter = ISO8601DateFormatter()
    //        return dateFormatter.date(from:LastSeen!)!
    //    }
    func getLastSeenDate() -> String {
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        // date format getting from server
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
        
        let date = dateFormatter.date(from: LastMessageDateTime!)!
        //date format you want
        dateFormatter.dateFormat = "HH:mm"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    func  isOnline() -> Bool {
        return online!;
    }
    
    //    mutating func getUserImageFilePath() -> String {
    //           if(UserImageFilePath == nil){
    //               return WSConstants.UserImgePath
    //           }
    //        if(UserImageFilePath!.count < 6){
    //               return WSConstants.UserImgePath;}
    //        if(UserImageFilePath!.starts(with: "~")){
    //            let resulte = UserImageFilePath!.replacingOccurrences(of: "~", with: WSConstants.siteurl)
    //               return WSConstants.thumbanailprofile + resulte;
    //           }
    //        return WSConstants.thumbanailprofile + UserImageFilePath!;
    //
    //       }
    
    mutating func getUserImageFilePath() -> String {
        if(UserImageFilePath == nil){
            return WSConstants.UserImgePath
        }
        if(!UserImageFilePath!.starts(with: "~") && !UserImageFilePath!.starts(with: "http") ){
            return WSConstants.UserImgePath
        }
        if(UserImageFilePath!.count < 6){
            return WSConstants.UserImgePath;}
        if(UserImageFilePath!.starts(with: "~")){
            let resulte = UserImageFilePath!.replacingOccurrences(of: "~", with: WSConstants.siteurl)
            return WSConstants.thumbanailprofile + resulte;
        }
        return WSConstants.thumbanailprofile + UserImageFilePath!;
        
    }
    
    mutating func setUserImageFilePath(userImageFilePath : String) {
        self.UserImageFilePath = userImageFilePath;
    }
    
    func  getEmail() -> String {
        return Email ?? ""
    }
    
}
