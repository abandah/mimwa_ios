//
//  ObjectNull.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 28/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
struct ObjectNull :Codable{
    let __AccessToken : String?
    enum CodingKeys: String, CodingKey {
        case __AccessToken = "__AccessToken"
        
    }
}
