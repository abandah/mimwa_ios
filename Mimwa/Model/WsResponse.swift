//
//  OfferModel.swift
//  OfferSwiper
//
//  Created by Lenvosoft on 23/01/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation


struct WsResponse<T : Codable>: Codable {

    let Value: Value<T>?
    let Status: Int?
    let StatusMessage : String?

    enum CodingKeys: String, CodingKey {
        case Value = "Value"
        case Status = "Status"
        case StatusMessage = "StatusMessage"
    }

}

// MARK: - DataFromServer
struct Value<T : Codable>: Codable {
    let Table: [T]?
    let OutPut: [T]?

    enum CodingKeys: String, CodingKey {
        case Table = "Table"
         case OutPut = "OutPut"
    }
}

