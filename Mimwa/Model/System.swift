//
//  System.swift
//  Mimwa
//
//  Created by Lenvosoft on 30/09/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import Foundation
struct System : Codable{
    let LinkedSystemId : Int
    let ClientId : Int
    let SystemName : String
    let SystemLevelOnly : Bool = true
    let ClientLevelOnly : Bool = true
    let HideMobileNumber : Bool = true
    let AllowSearchByName : Bool = true
    let AllowUserToUpdateInformation : Bool = true
    let AllowAddingContacts : Bool = true
    let AllowExternalInvitation : Bool = true

    enum CodingKeys: String, CodingKey {
        case LinkedSystemId = "LinkedSystemId"
        case ClientId = "ClientId"
        case SystemName = "SystemName"
        case SystemLevelOnly = "SystemLevelOnly"
        case ClientLevelOnly = "ClientLevelOnly"
        case HideMobileNumber = "HideMobileNumber"
        case AllowSearchByName = "AllowSearchByName"
        case AllowUserToUpdateInformation = "AllowUserToUpdateInformation"
        case AllowAddingContacts = "AllowAddingContacts"
        case AllowExternalInvitation = "AllowExternalInvitation"
    }}
