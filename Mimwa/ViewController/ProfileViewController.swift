//
//  ProfileViewController.swift
//  Mimwa
//
//  Created by Lenvosoft on 06/05/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var outletName: UILabel!
    @IBOutlet weak var outletphonenumber: UILabel!
    @IBOutlet weak var outletProfilePic: UIButton!
    @IBOutlet weak var outletdintmessage: UILabel!
    @IBOutlet weak var outletEditNameButton: UIButton!

    @IBOutlet weak var outletloader: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        showNavigationBar(isHidden: false)
        ApplyDataToProfile()


        // Do any additional setup after loading the view.
    }
    func showloader(show : Bool){
        if(show)
        {
            outletloader.startAnimating()
            outletloader.isHidden = false
        }else{
            outletloader.stopAnimating()
            outletloader.isHidden = true
        }
    }
    func ApplyDataToProfile(){
        if((AppShared.system?.AllowUserToUpdateInformation == false)){
            outletdintmessage.text = "use \(AppShared.system?.SystemName ?? "") to change yor information"
            outletEditNameButton.isHidden = true
            outletProfilePic.isEnabled = false
        }else{
        outletdintmessage.isHidden = true
        }
        let isEmpty = (AppShared.user?.UserDisplayName?.isEmpty)!
        if(isEmpty){
           
        }else{
            outletName.text = AppShared.user?.UserDisplayName
        }
        outletphonenumber.text = AppShared.user?.UserId
        let url : URL = URL(string: (AppShared.user?.getUserImageFilePath())!)!
        if (url .isFileURL){
            outletProfilePic.kf.setBackgroundImage(with: url, for: .normal)
        }else{

        }

       

        self.showloader(show: false)
    }
    
    @IBAction func ActionClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    @IBAction func ActionLogout(_ sender: Any) {
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to Logout ?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "NO THANKS", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "PROCEED", style: .default, handler: { action in
            UserDefaults.standard.setValue(nil, forKey:AppSharedKeys.AuthCodeKey)
            UserDefaults.standard.setValue(nil, forKey: AppSharedKeys.userIdKey)
            self.dismiss(animated: true, completion: nil)
        }))

        self.present(alert, animated: true)
    }

    @IBAction func ActionEditname(_ sender: Any) {
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Edit Name", message: "Enter a new name", preferredStyle: .alert)

        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.text = AppShared.user?.UserDisplayName
        }

        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField : String = (alert?.textFields![0].text)!
            if(textField.isEmpty){
                return
            }
            self.updateUserInformation(UserDiplayName: textField,UserEmail: (AppShared.user?.Email)!)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func ActionprofilepicClick(_ sender: Any) {
        ImagePickerManager().pickImage(self){ image,imgUrl in
            //here is the image
            //self.circularImage.image = image
            let imges  = [image]
            self.UploadImage(image: imges)
            //self.pictures.append((imgUrl?.absoluteString)!)
            // self.photo1ViewOutlet.isHidden = true
            //self.UpdateUserPicture(imges: imges)
        }
    }
    func updateUserInformation(UserDiplayName : String ,UserEmail : String) {
        self.showloader(show: true)
        let c : Call = Call<ObjcetBasic>()
        c.Operation = "CustomSP"
        c.Model = "Users_updateUserInformation"
        c.InputObject = Array (arrayLiteral: ["UserId":AppShared.UserId],
                         ["UserDiplayName": UserDiplayName],
                         ["UserEmail": UserEmail],
                         ["SystemId" : AppShared.SystemID],
                         ["ClientId" : AppShared.ClientId])
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<ObjcetBasic>?, apiError) in

            if apiError == nil{
                AppShared.user?.UserDisplayName = UserDiplayName
                self.ApplyDataToProfile()
                // AppShared.user?.UserDisplayName = UserDiplayName
                if (result?.StatusMessage) != nil{
                    //print(message)

                }else{
                    self.showloader(show: false)

                }

            }else{
                self.showloader(show: false)

            }
        }
    }
    func UploadImage(image : [UIImage]) {
           self.showloader(show: true)
           let c : Call = Call<ObjcetBasic>()
        c.uploadImageServer(imagesData: image) {(result: WsResponse<ObjcetBasic>?, apiError) in

               if apiError == nil{
                if let message  = result?.Value?.Table![0].FilePath{
                    self.updateUserInformation(url: message)
                   }else{
                       self.showloader(show: false)
                   }

               }else{
                   self.showloader(show: false)

               }
           }
       }
    func updateUserInformation(url : String) {
        self.showloader(show: true)
        let c : Call = Call<ObjcetBasic>()
        c.Operation = "CustomSP"
        c.Model = "Users_updateUserImage"
        c.InputObject = Array (arrayLiteral: ["UserId":AppShared.UserId],
                         ["UserImage": url],
                         ["SystemId": AppShared.SystemID],
                         ["ClientId": AppShared.ClientId])
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<ObjcetBasic>?, apiError) in

            if apiError == nil{
                AppShared.user?.UserImageFilePath = url
                self.ApplyDataToProfile()

            }else{
                self.showloader(show: false)

            }
        }
    }
}
