//
//  QRViewController.swift
//  Mimwa
//
//  Created by Lenvosoft on 12/11/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit

class QRViewController: UIViewController {
    weak var delegate: QRScannerViewDelegate?

    @IBOutlet weak var scannerView: QRScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if !scannerView.isRunning {
            scannerView.startScanning()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }
    @IBAction func actionClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}


extension QRViewController: QRScannerViewDelegate {
    func qrScanningDidStop() {
        delegate?.qrScanningDidStop()
        scannerView.startScanning()
        self.dismiss(animated: true, completion: nil)
    }

    func qrScanningDidFail() {
        delegate?.qrScanningDidFail()
        scannerView.startScanning()
        self.dismiss(animated: true, completion: nil)
       // presentAlert(withTitle: "Error", message: "Scanning Failed. Please try again")
    }

    func qrScanningSucceededWithCode(_ str: String?) {

        self.dismiss(animated: true, completion: nil)
        delegate?.qrScanningSucceededWithCode(str)
       // self.qrData = QRData(codeString: str)
    }



}

