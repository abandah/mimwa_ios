//
//  ChatViewController.swift
//  Mimwa
//
//  Created by Lenvosoft on 12/10/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit
import MapKit
import ReverseExtension
import LocationPicker
import CoreLocation

class ChatViewController: UIViewController,QRScannerViewDelegate, NFCResultDelegets {
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var buttonSend: UIButton!
    @IBOutlet weak var inputText: UITextField!
    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var outletloader: UIActivityIndicatorView!

    var list : [Message] = [Message]()
    var currentPage : Int = 0
       var isLoadingList : Bool = false
    var user: User? {
        didSet {
            AppShared.currentUser = (user?.getId()) ?? ""
            configureView()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        AppShared.currentUser = ""

    }

    //    let chatMessages = [
    //        [
    //            ChatMessage(text: "Here's my very first message", isIncoming: true, date: Date.dateFromCustomString(customString: "08/03/2018")),
    //            ChatMessage(text: "I'm going to message another long message that will word wrap", isIncoming: true, date: Date.dateFromCustomString(customString: "08/03/2018")),
    //        ],
    //        [
    //            ChatMessage(text: "I'm going to message another long message that will word wrap, I'm going to message another long message that will word wrap, I'm going to message another long message that will word wrap", isIncoming: false, date: Date.dateFromCustomString(customString: "09/15/2018")),
    //            ChatMessage(text: "Yo, dawg, Whaddup!", isIncoming: false, date: Date()),
    //            ChatMessage(text: "This message should appear on the left with a white background bubble", isIncoming: true, date: Date.dateFromCustomString(customString: "09/15/2018")),
    //        ],
    //        [
    //            ChatMessage(text: "Third Section message", isIncoming: true, date: Date.dateFromCustomString(customString: "10/31/2018"))
    //        ]
    //    ]

    //    let messagesFromServer = [
    //        ChatMessage(text: "Here's my very first message", isIncoming: true, date: Date.dateFromCustomString(customString: "08/03/2018")),
    //        ChatMessage(text: "I'm going to message another long message that will word wrap", isIncoming: true, date: Date.dateFromCustomString(customString: "08/03/2018")),
    //        ChatMessage(text: "I'm going to message another long message that will word wrap, I'm going to message another long message that will word wrap, I'm going to message another long message that will word wrap", isIncoming: false, date: Date.dateFromCustomString(customString: "09/15/2018")),
    //        ChatMessage(text: "Yo, dawg, Whaddup!", isIncoming: false, date: Date()),
    //        ChatMessage(text: "This message should appear on the left with a white background bubble", isIncoming: true, date: Date.dateFromCustomString(customString: "09/15/2018")),
    //        ChatMessage(text: "Third Section message", isIncoming: true, date: Date.dateFromCustomString(customString: "10/31/2018"))
    //    ]

    //    fileprivate func attemptToAssembleGroupedMessages() {
    //        print("Attempt to group our messages together based on Date property")
    //
    //        let groupedMessages = Dictionary(grouping: messagesFromServer) { (element) -> Date in
    //            return element.date.reduceToMonthDayYear()
    //        }
    //
    //        // provide a sorting for your keys somehow
    //        let sortedKeys = groupedMessages.keys.sorted()
    //        sortedKeys.forEach { (key) in
    //            let values = groupedMessages[key]
    //            chatMessages.append(contentsOf: values ?? [])
    //        }
    //
    //    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        if(!textField.text!.isEmpty){
            buttonSend.backgroundColor = myColors.subblue
        }else{
            buttonSend.backgroundColor = myColors.lightlightgray
        }
    }
    @IBAction func buttonSend_Action(_ sender: Any) {
        let s = inputText.text
        inputText.text = ""
        Messages_AddMessage(messagetext: s!,type: 1)
        buttonSend.backgroundColor = myColors.lightlightgray
    }
    @IBAction func buttonAttach_Action(_ sender: Any) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
            self.view.endEditing(true)
        }

        ImagePickerManager().pickImage(self){ image,imgUrl in
            //here is the image
            //self.circularImage.image = image
            let imges  = [image]
            self.UploadImage(image: imges)
            //self.pictures.append((imgUrl?.absoluteString)!)
            // self.photo1ViewOutlet.isHidden = true
            //self.UpdateUserPicture(imges: imges)
        }
    }
    func UploadImage(image : [UIImage]) {

           self.showloader(show: true)
           let c : Call = Call<ObjcetBasic>()
        c.uploadImageServer(imagesData: image) {(result: WsResponse<ObjcetBasic>?, apiError) in

               if apiError == nil{
                if let message  = result?.Value?.Table![0].FilePath{
                    self.Messages_AddMessage(messagetext: message, type: 2)
                   }else{
                       self.showloader(show: false)
                   }

               }else{
                   self.showloader(show: false)

               }
           }
       }
    let locationManager = CLLocationManager()
    func qrScanningDidFail() {

    }

    func qrScanningSucceededWithCode(_ str: String?) {
        self.Messages_AddMessage(messagetext: str!, type: 10)

    }

    func qrScanningDidStop() {

    }
    @IBAction func button_Location_Send(_ sender: Any) {

        let actionSheet = UIAlertController.init(title: "Please choose a do you want to do", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction.init(title: "Location", style: UIAlertAction.Style.default, handler: { (action) in
            self.attachLocation()
        }))
        actionSheet.addAction(UIAlertAction.init(title: "QR / BarCode", style: UIAlertAction.Style.default, handler: { (action) in
            self.performSegue(withIdentifier: "OpenQr",sender: self)
        }))
//        actionSheet.addAction(UIAlertAction.init(title: "NFC", style: UIAlertAction.Style.default, handler: { (action) in
//            self.performSegue(withIdentifier: "OpenNRC",sender: self)
//        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (action) in
            print("hgjhgjhg")
            // self.dismissViewControllerAnimated(true, completion: nil) is not needed, this is handled automatically,
            //Plus whatever method you define here, gets called,
            //If you tap outside the UIAlertController action buttons area, then also this handler gets called.
        }))
        //Present the controller
        self.present(actionSheet, animated: true, completion: nil)


    }
    func attachLocation(){
        locationManager.requestWhenInUseAuthorization()
        let locStatus = CLLocationManager.authorizationStatus()
           switch locStatus {
              case .notDetermined:
                 locationManager.requestWhenInUseAuthorization()
              return
              case .denied, .restricted:
                 let alert = UIAlertController(title: "Location Services are disabled", message: "Please enable Location Services in your Settings", preferredStyle: .alert)
                 let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                 alert.addAction(okAction)
                 present(alert, animated: true, completion: nil)
              return
              case .authorizedAlways, .authorizedWhenInUse:
              break
           @unknown default:
            break
           }


        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
            self.view.endEditing(true)
        }



        let locationPicker = LocationPickerViewController()

        // you can optionally set initial location
        let placemark = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: 37.331686, longitude: -122.030656), addressDictionary: nil)
        _ = Location(name: "1 Infinite Loop, Cupertino", location: nil, placemark: placemark)
       // locationPicker.location = location

        // button placed on right bottom corner
        locationPicker.showCurrentLocationButton = true // default: true

        // default: navigation bar's `barTintColor` or `UIColor.white`
        //locationPicker.currentLocationButtonBackground = .systemGray2
        locationPicker.currentLocationButtonBackground = UIColor.rgb(red: 174, green: 174, blue: 178)

        // ignored if initial location is given, shows that location instead
        locationPicker.showCurrentLocationInitially = true // default: true

        locationPicker.mapType = .standard // default: .Hybrid

        // for searching, see `MKLocalSearchRequest`'s `region` property
        locationPicker.useCurrentLocationAsHint = true // default: false

        locationPicker.searchBarPlaceholder = "Search places" // default: "Search or enter an address"

        locationPicker.searchHistoryLabel = "Previously searched" // default: "Search History"

        // optional region distance to be used for creation region when user selects place from search results
        locationPicker.resultRegionDistance = 500 // default: 600

        locationPicker.completion = { location in
            let mes = String((location?.coordinate.latitude)!) + "," + String((location?.coordinate.longitude)!)
            self.Messages_AddMessage(messagetext: mes,type: 5)

        }

        navigationController?.pushViewController(locationPicker, animated: true)

    }
    func showloader(show : Bool){
        if(show)
        {
            outletloader.startAnimating()
            outletloader.isHidden = false
        }else{
            outletloader.stopAnimating()
            outletloader.isHidden = true
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showimage","showimage2":

            guard
                //     let detailViewController = (segue.destination as? UINavigationController)?.topViewController as? DetailViewController
                let vc = segue.destination as? ViewImageViewController
            else {
                return
            }
            let mess = list[tableview.indexPathForSelectedRow!.row]
            vc.message = mess
            return
        case "OpenQr":
            guard
                let vc = segue.destination as?  QRViewController
            else {
                return
            }
            vc.delegate = self
            return
        case "OpenNRC":
//            guard
//                let vc = segue.destination as?  NFCViewController
//            else {
//                return
//            }
//            vc.delegate = self
            return
        default:
            return

        }

        //performSegue(withIdentifier: "OpenQr",sender: self)


    }
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        buttonSend.imageEdgeInsets = UIEdgeInsets(top: 8, left: 5, bottom: 5, right: 8)
        self.showloader(show: false)
        sendView.addTopBorderWithColor(color: myColors.lightgray!, width: 1)
        inputText.addTarget(self, action: #selector(ChatViewController.textFieldDidChange(_:)), for: .editingChanged)
        showNavigationBar(isHidden: false)
        NotificationCenter.default.addObserver(self,selector:#selector(handleObservedNotidaction(sender:)),name:NSNotification.Name(rawValue: "NupdateNotification"),
                                               object: nil)
        //  attemptToAssembleGroupedMessages()

        // navigationItem.title = "Messages"
        //   navigationController?.navigationBar.prefersLargeTitles = true

        tableview.re.delegate = self
        tableview.dataSource = self
        // tableview.register(ChatMessageCell.self, forCellReuseIdentifier: "Cell")
        tableview.re.scrollViewDidReachTop = { scrollView in
                    print("scrollViewDidReachTop")
                }
        tableview.re.scrollViewDidReachBottom = { scrollView in
                    print("scrollViewDidReachBottom")
                }
        tableview.separatorStyle = .none
        tableview.backgroundColor = UIColor(white: 0.95, alpha: 1)
        configureView()

    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }

    class DateHeaderLabel: UILabel {

        override init(frame: CGRect) {
            super.init(frame: frame)

            backgroundColor = .black
            textColor = .white
            textAlignment = .center
            translatesAutoresizingMaskIntoConstraints = false // enables auto layout
            font = UIFont.boldSystemFont(ofSize: 14)
        }

        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        override var intrinsicContentSize: CGSize {
            let originalContentSize = super.intrinsicContentSize
            let height = originalContentSize.height + 12
            layer.cornerRadius = height / 2
            layer.masksToBounds = true
            return CGSize(width: originalContentSize.width + 20, height: height)
        }

    }
    func Messages_GetConversation_ByRecepientId(PageNumber : Int) {
        let c : Call = Call<Message>()
        c.Operation = "CustomSP"
        c.Model = "Messages_GetConversation_ByRecepientId"
        c.InputObject = Array (arrayLiteral:
                                ["UserId":AppShared.UserId],
                               ["RecepientId": user?.getId() ?? ""] ,
                               ["SystemId" :AppShared.SystemID ],
                               ["ClientId" :AppShared.ClientId ],
                               ["PageNumber" :PageNumber],
                               ["PageSize" :10],
                               ["IsReversed" : true]
        )
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<Message>?, apiError) in

            if apiError == nil{
                self.isLoadingList = false
                if( PageNumber == 0)
                {
                    self.list.removeAll()
                    self.tableview.reloadData()
                    self.list = (result?.Value?.Table!)!
                }else{
                    self.list += (result?.Value?.Table!)!
                }
               // let li = (result?.Value?.Table!)!//
                //self.addElements(message:li)

                // print(self.list.count)
                self.tableview.reloadData()
               // self.tableview.setContentOffset(CGPoint(x: 0, y: CGFloat.greatestFiniteMagnitude), animated: false)
                if( PageNumber == 0)
                {
               // DispatchQueue.main.async {
                    //    let indexPath = IndexPath(row: self.list.count-1, section: 0)
                    //    self.tableview.scrollToRow(at: indexPath, at: .bottom, animated: false)
                  //  }
                }


            }else{

            }
        }
    }
    func Messages_AddMessage(messagetext : String,type :Int) {
        self.showloader(show: false)
        let c : Call = Call<Message>()
        c.Operation = "CustomSP"
        c.Model = "Messages_AddMessage"
        c.InputObject = Array (arrayLiteral:
                                ["UserId":AppShared.UserId],
                               ["SystemId" :AppShared.SystemID ],
                               ["ClientId" :AppShared.ClientId ],
                               ["RecepientId": user?.getId() ?? ""] ,
                               ["FK_MessageTypeID" :type],
                               ["Message" :messagetext]
        )
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<Message>?, apiError) in

            if apiError == nil{
                let Tempitem = (result?.Value?.Table!)![0]
                let message : Message = Message(id : Int(Tempitem.MessageId))
                message.MessageID = Int(Tempitem.MessageId)
                message.Message = messagetext
                message.RecepientID = self.user?.getId()
                message.SenderID = AppShared.UserId
                message.FK_MessageTypeID = type
                message.IsIncoming = 0
                message.setTodatDate()
                self.addOnElement(message: message)
            }else{

            }
        }
    }
    func addElements(message : [Message]){
//        if(message.count == 0)
//        {
//            return
//        }
//        var m:[Message] = message
//        addOnElement(message: message[0])
//        m.remove(at: 0)
//        addElements(message: m)

    }
    func addOnElement(message : Message) {
          //  list.append(message)
        list.insert(message, at: 0)
        self.tableview.reloadData()
        //tableview.beginUpdates()
      //  tableview.re.insertRows(at: [IndexPath(row: list.count - 1, section: 0)], with: .automatic)
      //  tableview.endUpdates()
        }
    @objc func handleObservedNotidaction(sender: NSNotification){

        let noti = NotificationBodyJSON.FromJSON(sender: sender)
        if(noti.isSameUserANDSameSYStem(CurrentUser: (user?.getId())!)){
            let message : Message = Message(id : Int(noti.MessageId)!)
            message.MessageID = Int(noti.MessageId)
            message.Message = noti.Content
            message.RecepientID = AppShared.UserId
            message.SenderID = noti.SenderId
            message.FK_MessageTypeID = Int(noti.mType)
            message.IsIncoming = 1
            message.setTodatDate()
            addOnElement(message: message)
            DispatchQueue.main.async {
                self.Messages_IsSeen(MessageId: Int(message.MessageID))
            }
        }

    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func configureView() {
        if(user != nil){
            self.title = user?.getName()
            Messages_GetConversation_ByRecepientId(PageNumber: 0)
        }

    }

    func Messages_IsSeen(MessageId : Int) {
        let c : Call = Call<Message>()
        c.Operation = "CustomSP"
        c.Model = "Messages_IsSeen"
        c.InputObject = Array (arrayLiteral:
                                ["UserId":AppShared.UserId],
                               ["MessageId": MessageId]
        )
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<Message>?, apiError) in

            if apiError == nil{


            }else{

            }
        }
    }
}

extension ChatViewController : UITableViewDelegate, UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chatMessage = list[indexPath.row]
        var identifier = ""

        switch chatMessage.FK_MessageTypeID {
        case 1:
            identifier = chatMessage.IsIncoming != 1 ? "S_Text" : "R_Text"
        default:
            identifier = chatMessage.IsIncoming != 1 ? "S_Imge":"R_Imge"
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! ParentTableViewCell
        cell.chatMessage = chatMessage
        //print(indexPath.row)
        return cell


    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
            self.view.endEditing(true)
        }
        let chatMessage = list[indexPath.row]
        if(chatMessage.FK_MessageTypeID == 5){
            let loc = chatMessage.Message.components(separatedBy:",")

            let lat1 : NSString = loc[0] as NSString
            let lng1 : NSString = loc[1] as NSString

            let latitude:CLLocationDegrees =  lat1.doubleValue
            let longitude:CLLocationDegrees =  lng1.doubleValue

            let regionDistance:CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
            let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = "jhjkhkj"
            mapItem.openInMaps(launchOptions: options)
        }else if (chatMessage.FK_MessageTypeID == 2){
            performSegue(withIdentifier: "showimage",sender: self)
        }
        //        if(chatMessage.FK_MessageTypeID == 2){
        //            let showAlert = UIAlertController(title: "Demo Alert", message: nil, preferredStyle: .alert)
        //            let imageView = UIImageView(frame: CGRect(x: 10, y: 50, width: 500, height: 400))
        //            imageView.kf.setStringImage(with:chatMessage.get_MessagePic_Full())
        //
        //            //  imageView.image = image // Your image here...
        //            showAlert.view.addSubview(imageView)
        //            let height = NSLayoutConstraint(item: showAlert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 570)
        //            let width = NSLayoutConstraint(item: showAlert.view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 420)
        //            showAlert.view.addConstraint(height)
        //            showAlert.view.addConstraint(width)
        //            showAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
        //                // your actions here...
        //            }))
        //            self.present(showAlert, animated: true, completion: nil)
        //        }
        //           let next:PurchasesViewItemViewController = storyboard?.instantiateViewController(withIdentifier: "PurchasesViewItemViewController") as! PurchasesViewItemViewController
        //           next.wishlist = list[indexPath.row]
        //           self.navigationController?.pushViewController(next, animated: true)

    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.contentOffset.y <= 0){
            return
        }
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
                   self.isLoadingList = true
                  self.loadMoreItemsForList()
               }
    }
    func loadMoreItemsForList(){
        currentPage += 1
        Messages_GetConversation_ByRecepientId(PageNumber: currentPage)
    }


    //    private override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //        if let firstMessageInSection = chatMessages[section].first {
    //            let dateFormatter = DateFormatter()
    //            dateFormatter.dateFormat = "MM/dd/yyyy"
    //            let dateString = dateFormatter.string(from: firstMessageInSection.date)
    //
    //            let label = DateHeaderLabel()
    //            label.text = dateString
    //
    //            let containerView = UIView()
    //
    //            containerView.addSubview(label)
    //            label.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
    //            label.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
    //
    //            return containerView
    //
    //        }
    //        return nil
    //    }


    //    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

    //
    //        return "Section: \(Date())"
    //    }

    //    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        return chatMessages[section].count
    //
    //    }


    func onDataResult(result: String) {
        Messages_AddMessage(messagetext: result,type: 11)
    }
}










