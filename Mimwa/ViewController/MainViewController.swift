import UIKit
import MessageUI


class MainViewController: UIViewController ,OpenChatListener , MFMessageComposeViewControllerDelegate  {



    var senderId : String?{
        didSet{
            onHaveNumber(userid: senderId!)
        }
    }

    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var outlityouHaveNoContact: UIButton!
    @IBOutlet weak var outletYoudidnotsetyourname: UIButton!
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }

    var timer: Timer?
    func onHaveNumber(userid : String) {
        self.outletButtonAdd.loadingIndicator(true)

        //showLoader(show: true)
        let c : Call = Call<User>()
        c.Operation = "CustomSP"
        c.Model = "Users_select_ById"
        c.InputObject = Array (arrayLiteral: ["UserId":userid],
                               ["OnlyVerified":false],
                               ["FK_ClientId":AppShared.ClientId],
                               ["FK_SystemID":AppShared.SystemID])
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<User>?, apiError) in

            if apiError == nil{
                let user = result?.Value?.Table!
                if(user!.count <= 0){
                    if(AppShared.system?.AllowExternalInvitation == true && MFMessageComposeViewController.canSendText() == true){
                        self.showInviteMessage(phonenumber: userid)
                    }else{
                        self.showToast(message: "Mobile number not found",tostType: .Info)

                    }
                    self.outletButtonAdd.loadingIndicator(false)
                    return
                }else{
                    self.toCallUser = user![0]
                    self.timer = Timer.scheduledTimer(timeInterval: 1,
                                                      target: self,
                                                      selector: #selector(self.eventWith(timer:)),
                                                      userInfo: [ "foo" : "bar" ],
                                                      repeats: false)

                }


            }else{

            }
        }


        // detailViewController.candy = item
    }
    func showInviteMessage(phonenumber : String ){
        let alert = UIAlertController(title: "Alert", message: "Mobile number not found on meemwaw would you like to send an invitation.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in


        }))
        alert.addAction(UIAlertAction(title: "Invite", style: .default, handler: { action in
            let m : String = "Hi i am using meemwaw to get in touch with you please download the app from"
            if(MFMessageComposeViewController.canSendText()){

                let controller = MFMessageComposeViewController()
                controller.body = m
                controller.recipients = [phonenumber]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        }))

        self.present(alert, animated: true, completion: nil)
    }
    @objc func eventWith(timer: Timer!) {
        self.outletButtonAdd.loadingIndicator(false)
        self.performSegue(withIdentifier: "ShowDetailSegue",sender: self)
    }

    @IBOutlet weak var outletbuttonmore: UIButton!
    var list : [User] = [User]()
    var actionSheet :UIAlertController? = nil
    var LastItem = 0;
    let cellSpacingHeight: CGFloat = 5
    var systems : [System] = [System]()
    @IBOutlet weak var outlettitle: UILabel!
    @IBOutlet weak var ouletProfilePicButton: UIImageView!
    @IBOutlet weak var outletButtonAdd: UIButton!


    @IBOutlet weak var outletSystem: UIButton!

    var toCallUser : User!
    var status : Int = 0

    @IBOutlet var outlistList: UITableView!
    override func viewWillAppear(_ animated: Bool) {
        showNavigationBar(isHidden: true)
        NotificationCenter.default.addObserver(self,selector:#selector(handleObservedNotidaction(sender:)),name:NSNotification.Name(rawValue: "updateNotification"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,selector:#selector(handleObservedNNotidaction(sender:)),name:NSNotification.Name(rawValue: "NupdateNotification"),
                                               object: nil)
        if let newNotificationObject = UserDefaults.standard.value(forKey: "newNotfication") as? [AnyHashable : Any]{
            if AppShared.fromNotification! {
                let notification = NotificationBodyJSON.FromJSON(dic: newNotificationObject)
                self.handlenewNotidaction(sender: notification)
                //let userinfo = newNotificationObject.request.content.userInfo
                //self.handleObservedNotidaction(sender: newNotificationObject)


                UserDefaults.standard.removeObject(forKey: "newNotfication")
                AppShared.fromNotification = false
                AppShared.lunchFromNotification = false
            }
        }
    }
    @IBAction func actionnoContactClick(_ sender: Any) {

        if(AppShared.system?.AllowSearchByName == true){
            performSegue(withIdentifier: "mainToSearch",sender: self)
        }else{
            performSegue(withIdentifier: "MainToAddPhonenumber",sender: self)
        }
    }
    @IBAction func actionAddContact(_ sender: Any) {


        //1. Create the alert controller.
        let alert = UIAlertController(title: "Edit Name", message: "Enter a new name", preferredStyle: .alert)

        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.text = AppShared.user?.UserDisplayName
        }

        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField : String = (alert?.textFields![0].text)!
            if(textField.isEmpty){
                return
            }
            self.updateUserInformation(UserDiplayName: textField,UserEmail: (AppShared.user?.Email)!)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    func updateUserInformation(UserDiplayName : String ,UserEmail : String) {
        //self.showloader(show: true)
        let c : Call = Call<ObjcetBasic>()
        c.Operation = "CustomSP"
        c.Model = "Users_updateUserInformation"
        c.InputObject = Array (arrayLiteral: ["UserId":AppShared.UserId],
                         ["UserDiplayName": UserDiplayName],
                         ["UserEmail": UserEmail],
                         ["SystemId" : AppShared.SystemID],
                         ["ClientId" : AppShared.ClientId])
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<ObjcetBasic>?, apiError) in

            if apiError == nil{
                AppShared.user?.UserDisplayName = UserDiplayName
                self.applyUserToApp(s: AppShared.user!,noti: nil)
                // AppShared.user?.UserDisplayName = UserDiplayName
               // if let message  = result?.StatusMessage{

               // }else{
                   // self.showloader(show: false)

               // }

            }else{
               // self.showloader(show: false)

            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //showNavigationBar(isHidden: true)
        outletButtonAdd.imageEdgeInsets = UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50)
        topview.addBottomLine()
        outlistList.delegate = self
        outlistList.dataSource = self
        self.outletButtonAdd.isHidden = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        ouletProfilePicButton.isUserInteractionEnabled = true
        ouletProfilePicButton.addGestureRecognizer(tapGestureRecognizer)
        // outlistList.backgroundView = UIImageView(image: UIImage(named: "Background.png"))

    }

    @objc func handleObservedNotidaction(sender: NSNotification){

        let noti = NotificationBodyJSON.FromJSON(sender: sender)
        AppShared.SystemID = Int(noti.SystemId)!
        AppShared.ClientId = Int(noti.ClientId)!
      //  showToast(message: "dcdcdcdcdcdcdcd", tostType: .Error)
        Systems_SelectAll_LinkedSystems_Mobile(noti: noti)

    }
    @objc func handleObservedNNotidaction(sender: NSNotification){

        let noti = NotificationBodyJSON.FromJSON(sender: sender)
        AppShared.SystemID = Int(noti.SystemId)!
        AppShared.ClientId = Int(noti.ClientId)!
      //  showToast(message: "dcdcdcdcdcdcdcd", tostType: .Error)
        Systems_SelectAll_LinkedSystems_Mobile(noti: nil)

    }
    func handlenewNotidaction(sender: NotificationBodyJSON?){

        if(sender == nil)
        {
            return
        }
        // let noti = NotificationBodyJSON.FromJSON(sender: sender)
        AppShared.SystemID = Int(sender!.SystemId)!
        AppShared.ClientId = Int(sender!.ClientId)!
       // showToast(message: "dcdcdcdcdcdcdcd", tostType: .Error)
        Systems_SelectAll_LinkedSystems_Mobile(noti: sender)

    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "ShowDetailSegue":
            if(toCallUser == nil){
                toCallUser = list[outlistList.indexPathForSelectedRow!.row]
            }
            guard
                //     let detailViewController = (segue.destination as? UINavigationController)?.topViewController as? DetailViewController
                let detailViewController = segue.destination as? ChatViewController
            else {
                return
            }
            detailViewController.user = toCallUser
            toCallUser = nil
            return
        case "MainToAddPhonenumber":
            guard
                let vc = segue.destination as?  addPhoneViewController
            else {
                return
            }
            vc.delegate = self
            toCallUser = nil
            return
        case "mainToSearch":
            guard
                let vc = segue.destination as?  MasterViewController
            else {
                return
            }
            vc.delegate = self
            toCallUser = nil
            return

        default:
            return

        }

        //


    }

    @IBAction func ActionButtonAdd(_ sender: Any) {

        if(AppShared.system?.AllowSearchByName == true){
            performSegue(withIdentifier: "mainToSearch",sender: self)

        }else{

            //            let vc = addPhoneViewController()
            //            vc.modalPresentationStyle = .custom
            //            present(vc, animated: true, completion: nil)
            //            let vc2 = self.storyboard?.instantiateViewController(withIdentifier: "addPhoneViewController") as! addPhoneViewController
            //            self.present(vc2, animated: true, completion: nil)

            performSegue(withIdentifier: "MainToAddPhonenumber",sender: self)
        }
    }

    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        // let tappedImage = tapGestureRecognizer.view as! UIImageView
        performSegue(withIdentifier: "MenuToProfile",sender: self)



        // Your action
    }
    @IBAction func ActionSystemButton(_ sender: Any) {

        if(actionSheet == nil){
            return
        }

        //Present the controller
        self.present(actionSheet!, animated: true, completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        Systems_SelectAll_LinkedSystems_Mobile(noti: nil)
    }
    func Actionshowmenu(_ sender: Any) {

        let actionSheet = UIAlertController.init(title: "Please choose a do you want to do", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction.init(title: "Create Meeting", style: UIAlertAction.Style.default, handler: { (action) in

        }))
        actionSheet.addAction(UIAlertAction.init(title: "Choose Photo", style: UIAlertAction.Style.default, handler: { (action) in
            print("hgjhgjhg")
        }))
        actionSheet.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (action) in
            print("hgjhgjhg")
            // self.dismissViewControllerAnimated(true, completion: nil) is not needed, this is handled automatically,
            //Plus whatever method you define here, gets called,
            //If you tap outside the UIAlertController action buttons area, then also this handler gets called.
        }))
        //Present the controller
        self.present(actionSheet, animated: true, completion: nil)
    }
    func GetBySystemAndClientId(noti : NotificationBodyJSON?) {
        let c : Call = Call<User>()
        c.Operation = "CustomSP"
        c.Model = "ClientSystemDefaultChatUsers_GetBySystemAndClientId"
        c.InputObject = Array (arrayLiteral: ["UserId":AppShared.UserId], ["SystemId": AppShared.SystemID] , ["ClientId" :AppShared.ClientId ])
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<User>?, apiError) in

            if apiError == nil{
                self.list = (result?.Value?.Table!)!
                if(self.list.count <= 0){
                    self.outlistList.isHidden = true
                    self.outlityouHaveNoContact.isHidden = false
                }else{
                    self.outlistList.isHidden = false
                    self.outlityouHaveNoContact.isHidden = true
                    print(self.list.count)
                    self.outlistList.reloadData()
                    if(noti != nil){
                        self.onHaveNumber(userid: noti!.SenderId)
                    }
                }


            }else{

            }
        }
    }
    func Users_select_ById(noti : NotificationBodyJSON?) {
        //showLoader(show: true)
        let c : Call = Call<User>()
        c.Operation = "CustomSP"
        c.Model = "Users_select_ById"
        c.InputObject = Array (arrayLiteral: ["UserId":AppShared.UserId],
                               ["OnlyVerified":false],
                               ["FK_ClientId":AppShared.ClientId],
                               ["FK_SystemID":AppShared.SystemID])
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<User>?, apiError) in

            if apiError == nil{
                AppShared.user = result?.Value?.Table![0]
                self.applyUserToApp(s: AppShared.user!,noti: noti)
            }else{

            }
        }
    }
    func Systems_SelectAll_LinkedSystems_Mobile(noti : NotificationBodyJSON?) {
        //showLoader(show: true)
        if(noti != nil){
            AppShared.SystemID = Int(noti!.SystemId)!
            AppShared.ClientId = Int(noti!.ClientId)!
        }
        let c : Call = Call<System>()
        c.Operation = "CustomSP"
        c.Model = "Systems_SelectAll_LinkedSystems_Mobile"
        c.InputObject = Array (arrayLiteral: ["UserId":AppShared.UserId],
                               ["FK_SystemID":AppShared.SystemID],
                               ["FK_MainSystemId":1],
                               ["ClientId":AppShared.ClientId])
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<System>?, apiError) in

            if apiError == nil{
                self.systems = (result?.Value?.Table)!
                self.actionSheet = nil
                self.actionSheet = UIAlertController.init(title: "Please choose a do you want to do", message: nil, preferredStyle: .actionSheet)
                for s in self.systems {
                    if(s.LinkedSystemId == AppShared.SystemID && s.ClientId == AppShared.ClientId){
                        self.applySystemToApp(s: s)
                    }
                    self.actionSheet!.addAction(UIAlertAction.init(title:s.SystemName, style: UIAlertAction.Style.default, handler: { (action) in
                        self.applySystemToApp(s: s)
                        self.Users_select_ById(noti: noti);
                    }))
                }
                self.actionSheet!.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (action) in
                    // print("hgjhgjhg")
                    // self.dismissViewControllerAnimated(true, completion: nil) is not needed, this is handled automatically,
                    //Plus whatever method you define here, gets called,
                    //If you tap outside the UIAlertController action buttons area, then also this handler gets called.
                }))
                self.Users_select_ById(noti: noti);
            }else{

            }
        }
    }
    func applySystemToApp(s :System){
        AppShared.system = s
        AppShared.SystemID = s.LinkedSystemId
        AppShared.ClientId = s.ClientId
        self.outletSystem.setTitle(s.SystemName, for: .normal)
        self.outletButtonAdd.isHidden = ((AppShared.system?.AllowAddingContacts) == false)
    }
    func applyUserToApp( s :User,noti : NotificationBodyJSON?){
        AppShared.user = s
        var _ = ouletProfilePicButton.kf.setStringImage(with:AppShared.user!.getAvatar())

        let isEmpty = (AppShared.user?.UserDisplayName?.isEmpty)!
        if(isEmpty){
            outletButtonAdd.isEnabled = false
            outlistList.isHidden = true
            outletYoudidnotsetyourname.isHidden = false
        }else{
            outlettitle.text = s.UserDisplayName
            outletButtonAdd.isEnabled = true
            outlistList.isHidden = false
            outletYoudidnotsetyourname.isHidden = true
            self.GetBySystemAndClientId(noti: noti)
        }

        // out.kf.setStringImage(with:item.getAvatar())

    }
    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */

}

extension MainViewController : UITableViewDelegate, UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list.count
    }
    func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UserTableViewCell

        // add border and color
        // cell.backgroundColor = UIColor.clear
        //cell.layer.borderColor = UIColor.gray.cgColor
        //cell.layer.borderWidth = 1
        // cell.layer.cornerRadius = 0
        //cell.clipsToBounds = true
        let index : Int = indexPath.row
        var item : User = list[index]
        //           print(indexPath.row)
        var _ = cell.outletimage.kf.setStringImage(with:item.getAvatar())
        cell.outletname.text = item.getName()
        let lastmessage :String = item.LastMessage ?? ""
        cell.outletlastmessag.text = lastmessage
        let unread = item.NewMessages ?? 0
        if(unread <= 0){
            cell.outletunred.isHidden = true;
            cell.contentView.backgroundColor = .white
        }else{
            cell.outletunred.text = "\(unread)"
            cell.contentView.backgroundColor = myColors.lightlightgray

        }

        let lastmessagetime = item.getLastSeenDate()
        cell.outlettime.text = lastmessagetime




        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        //           let next:PurchasesViewItemViewController = storyboard?.instantiateViewController(withIdentifier: "PurchasesViewItemViewController") as! PurchasesViewItemViewController
        //           next.wishlist = list[indexPath.row]
        //           self.navigationController?.pushViewController(next, animated: true)

    }

}
