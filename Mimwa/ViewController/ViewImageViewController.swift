//
//  ViewImageViewController.swift
//  Mimwa
//
//  Created by Lenvosoft on 13/10/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit

class ViewImageViewController: UIViewController {


    @IBOutlet weak var imageView: UIImageView!

    var message :Message? {
        didSet {

            start()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        start()
        // Do any additional setup after loading the view.
    }
    func start(){
        if(message != nil && imageView != nil){
            var _ = imageView.kf.setStringImage(with: message?.get_MessagePic_Full())
    }
    }

}
