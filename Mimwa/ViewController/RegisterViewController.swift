//
//  RegisterViewController.swift
//  Mimwa
//
//  Created by Lenvosoft on 01/05/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit
import UIKit
import Alamofire
import PinCodeTextField
import FirebaseInstanceID

class RegisterViewController: UIViewController {
    @IBOutlet weak var loaderOutlet: UIActivityIndicatorView!
    @IBOutlet weak var pinCodeTextField: PinCodeTextField!
    @IBOutlet weak var textField: UITextField!
    var GCM : String?

    @IBOutlet weak var outletButtonVerify: UIButton!
    @IBOutlet weak var outleButtonNext: UIButton!

    @IBOutlet weak var outletButtonResend: UIButton!
    var deleget : Splashdeleget?;

    @IBOutlet weak var outletbuttonChangeMobile: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        showLoader(show: false)
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
                self.GCM = ""
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                self.GCM = result.token

            }
        }
        pinCodeTextField.keyboardType = .emailAddress
       
        self.textField.becomeFirstResponder()

    }

    override public var prefersStatusBarHidden: Bool {
        return false
    }

    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }


    @IBAction func actionButton(_ sender: Any) {
        let s : String = textField.text ?? ""
        if(!s.isEmpty){
            CreateUser(phonenumber: s)
        }

    }

    @IBAction func actionVerifyButton(_ sender: Any) {
        showLoader(show: true)
        guard let pincode = self.pinCodeTextField.text else{
            showLoader(show: false)
            return
        }
        if(pincode.length  != 5){
            showLoader(show: false)
            return
        }

        self.Users_VerifyUser(pin: pincode)
    }
    func showProgress(show : Bool) {
        if(show)
        {
            loaderOutlet.startAnimating()
            loaderOutlet.isHidden = false
        }else{
            loaderOutlet.stopAnimating()
            loaderOutlet.isHidden = true
        }
    }
    @IBAction func ActionButtonResend(_ sender: Any) {
        let s : String = textField.text ?? ""
        if(!s.isEmpty){
            CreateUser(phonenumber: s)
        }
    }
    
    @IBAction func ActionChangeMobile(_ sender: Any) {
        showenterPhonenumber()
    }
    
    func showPhonenumber(){

        pinCodeTextField.isHidden = true;
        textField.isHidden = false;
        outleButtonNext.isHidden = false
        outletButtonVerify . isHidden = true

    }
    var timerinterver : Int = 60
    var timer:Timer?
    func showPinCode(){

        timer = nil
        count = timerinterver
        showLoader(show: false)
        pinCodeTextField.isHidden = false;
        outletButtonResend.isHidden = false
        outletButtonResend.isEnabled = false
        outletbuttonChangeMobile.isHidden = false
        textField.isHidden = true;
        outleButtonNext.isHidden = true
        outletButtonVerify . isHidden = false
        self.pinCodeTextField.becomeFirstResponder()
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)

    }
    var count : Int = 60
    @objc func update() {
        if(count > 0) {
            count  -= 1
            outletButtonResend.setTitle("Resend After \(String(count))",for: .normal)
            if(count == 0){
                count = timerinterver
                timer!.invalidate()
                outletButtonResend.setTitle("Resend Code",for: .normal)
                outletButtonResend.isEnabled = true
            }
            //countDownLabel.text = String(count--)
        }else{
            count = timerinterver
                timer!.invalidate()
                outletButtonResend.setTitle("Resend Code",for: .normal)
                outletButtonResend.isEnabled = true
        }
    }
    func showenterPhonenumber(){
        timer!.invalidate()
        count = timerinterver
        showLoader(show: false)
        pinCodeTextField.isHidden = true;
        pinCodeTextField.text = ""
        outletButtonResend.isHidden = true
        outletbuttonChangeMobile.isHidden = true
        textField.isHidden = false;
        outleButtonNext.isHidden = false
        outletButtonVerify . isHidden = true
        self.textField.becomeFirstResponder()
    }


    func showLoader(show : Bool) {
        if(show){
            loaderOutlet.startAnimating()
        }else{
            loaderOutlet.stopAnimating()

        }
    }



    func saveDataToPref(stat : String) {

        //UserDefaults.standard.set(stat , forKey: "userId")

        //let s = UserDefaults.standard.value(forKey: "userId")

        // let userId = AppRefrence.sharedInstance.userId

        // let userId = AppShared.userId
    }
}
extension RegisterViewController: PinCodeTextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }

    func textFieldDidBeginEditing(_ textField: PinCodeTextField) {

    }

    func textFieldValueChanged(_ textField: PinCodeTextField) {
        let value = textField.text ?? ""
        if(value.length < 4){
            //self.nextPinCodeOutlet.isHidden = true
            return
        }
        // self.nextPinCodeOutlet.isHidden = false
    }

    func textFieldShouldEndEditing(_ textField: PinCodeTextField) -> Bool {
        return true
    }

    func textFieldShouldReturn(_ textField: PinCodeTextField) -> Bool {
        return true
    }

    func CreateUser(phonenumber : String ) {
        showLoader(show: true)
        let c : Call = Call<ObjcetBasic>()
        c.Operation = "CustomSP"
        c.Model = "Users_CreateUser"
        c.InputObject = Array (arrayLiteral: ["PhoneNumber":phonenumber],
                               ["GcmToken": GCM ?? ""])
        c.IdentityObject = nil
        c.accessToken = "Registration"
        //c.IsSecure = true
        c.Run() { [weak self]  (result: WsResponse<ObjcetBasic>?, apiError) in

            if apiError == nil{

                if let id  = result?.Value?.Table![0].PhoneNumber{

                    UserDefaults.standard.setValue(id, forKey:AppSharedKeys.userIdKey)
                    AppShared.UserId = id
                    self!.__AFG_AuthCode();
                }else{
                    self!.showLoader(show: false)

                }

            }else{
                self!.showLoader(show: false)

            }
        }
    }
    func __AFG_AuthCode() {
        showLoader(show: true)
        let c : Call = Call<ObjcetBasic>()
        c.GetAuthCode()
        c.Run() { [weak self]  (result: WsResponse<ObjcetBasic>?, apiError) in

            if apiError == nil{
                if let auth = result?.Value?.OutPut![0].__AuthCode{
                    UserDefaults.standard.setValue(auth, forKey:AppSharedKeys.AuthCodeKey)
                    AppShared.AuthCode = auth
                    self!.__AFG_ClientToken()
                }else{
                    self!.showLoader(show: false)
                }

            }else{
                self!.showLoader(show: false)
            }
        }
    }
    func __AFG_ClientToken() {
        showLoader(show: true)
        let c : Call = Call<ObjcetBasic>()
        c.GetClientToken()
        c.Run() { [weak self]  (result: WsResponse<ObjcetBasic>?, apiError) in

            if apiError == nil{
                if let auth = result?.Value?.OutPut![0].__ClientToken{
                    //UserDefaults.standard.setValue(auth, forKey:AppSharedKeys.AuthCodeKey)
                    AppShared.ClienToken = auth
                    self!.showPinCode()

                }else{
                    self!.showLoader(show: false)
                }

            }else{
                self!.showLoader(show: false)
            }
        }
    }
    func Users_VerifyUser(pin : String ) {

        //showLoader(show: true)
        let c : Call = Call<ObjcetBasic>()
        c.Operation = "CustomSP"
        c.Model = "Users_VerifyUser"
        c.InputObject = Array (arrayLiteral: ["UserId":AppShared.UserId], ["UserVerificationCode": pin])
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<ObjcetBasic>?, apiError) in

            if apiError == nil{

                if let message  = result?.Value?.Table![0].Status{
                    if(message > 0){
                        UserDefaults.standard.setValue(true, forKey:AppSharedKeys.verified)
                        self.showLoader(show: false)
                        self.dismiss(animated: true, completion: nil)
                        self.deleget?.restartViewControler()
                    }else{
                        self.showLoader(show: false)
                    }

                }else{
                    self.showLoader(show: false)

                }

            }else{
                self.showLoader(show: false)

            }
        }
    }
    func UpdateUserPicture(imges : [UIImage]){
        let c : Call = Call<WsResponse<ObjectNull>>()
        c.uploadImageServer(imagesData: imges) { (result: WsResponse<ObjectNull>?, apiError) in

            if apiError != nil{

            }else{

            }
        }

    }

}
