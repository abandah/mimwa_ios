//
//  SplashViewController.swift
//  Mimwa
//
//  Created by Lenvosoft on 01/05/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit
import FirebaseInstanceID

class SplashViewController: UIViewController,Splashdeleget {
    func restartViewControler() {
        startTimer()
    }

    var GCM : String?

    override func viewDidLoad() {
        super.viewDidLoad()


       // NotificationCenter.default.addObserver(self, selector: #selector(onResume), name:
         //   UIApplication.willEnterForegroundNotification, object: nil)
//        Messaging.messaging().token { token, error in
//          if let error = error {
//            print("Error fetching FCM registration token: \(error)")
//          } else if let token = token {
//            print("FCM registration token: \(token)")
//            self.fcmRegTokenMessage.text  = "Remote FCM registration token: \(token)"
//          }
//        }
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
                self.GCM = ""
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                self.GCM = result.token

            }
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // startTimer()

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        startTimer()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "SplashToLogin":

            guard
                //     let detailViewController = (segue.destination as? UINavigationController)?.topViewController as? DetailViewController
                let vc = segue.destination as? RegisterViewController
            else {
                return
            }
            //let mess = list[tableview.indexPathForSelectedRow!.row]
            vc.deleget = self
            return

        default:
            return

        }

        //


    }
    @objc func onResume() {

    }

    var timer: Timer?

    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 0,
                                     target: self,
                                     selector: #selector(eventWith(timer:)),
                                     userInfo: [ "foo" : "bar" ],
                                     repeats: false)
    }
    // Timer expects @objc selector
    @objc func eventWith(timer: Timer!) {
        if let auth = UserDefaults.standard.value(forKey: AppSharedKeys.AuthCodeKey) as? String {
            if let id = UserDefaults.standard.value(forKey: AppSharedKeys.userIdKey) as? String {
                guard let isVeryfied = UserDefaults.standard.value(forKey: AppSharedKeys.verified) as? Bool else {
                    performSegue(withIdentifier: "SplashToLogin",sender: self)
                    return
                }
                if ( isVeryfied ) {
                    AppShared.AuthCode = auth
                    AppShared.UserId = id

                    __AFG_ClientToken()
                    return
                }else{
                    performSegue(withIdentifier: "SplashToLogin",sender: self)
                }

            }
            else{
                performSegue(withIdentifier: "SplashToLogin",sender: self)

            }
        }else{
            performSegue(withIdentifier: "SplashToLogin",sender: self)
        }

    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        timer?.invalidate()
        timer = nil
        //stop list
    }
    func __AFG_ClientToken() {
        let c : Call = Call<ObjcetBasic>()
        c.GetClientToken()
        c.Run() { [weak self]  (result: WsResponse<ObjcetBasic>?, apiError) in

            if apiError == nil{
                if let auth = result?.Value?.OutPut![0].__ClientToken{
                    //UserDefaults.standard.setValue(auth, forKey:AppSharedKeys.AuthCodeKey)
                    AppShared.ClienToken = auth
                    self!.Users_select_ById()
                }else{
                    self!.performSegue(withIdentifier: "SplashToLogin",sender: self)
                }

            }else{
                self!.performSegue(withIdentifier: "SplashToLogin",sender: self)
            }
        }
    }
    func Users_select_ById() {
        //showLoader(show: true)
        let c : Call = Call<User>()
        c.Operation = "CustomSP"
        c.Model = "Users_select_ById"
        c.InputObject = Array (arrayLiteral: ["UserId":AppShared.UserId],
            ["OnlyVerified":false],
            ["FK_ClientId":AppShared.ClientId],
            ["FK_SystemID":AppShared.SystemID])
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<User>?, apiError) in

            if apiError == nil{

                if let message  = result?.StatusMessage{
                    if(message == "OK Your Request:" || message == "IsVarifed"){
                        AppShared.user = result?.Value?.Table![0]
                        if(!(AppShared.user?.IsPhoneVerified)!){
                            self.performSegue(withIdentifier: "SplashToLogin",sender: self)
                            return
                        }
                        if (self.GCM ?? "" == "") {
                            self.performSegue(withIdentifier: "SplashToMenu",sender: self)
                           // self.dismiss(animated: true, completion: nil)
                        }else{
                            self.GCM_UpdateByUserId(GCMId: self.GCM!)
                        }

                    }else{
                        self.performSegue(withIdentifier: "SplashToLogin",sender: self)
                    }

                }else{
                    self.performSegue(withIdentifier: "SplashToLogin",sender: self)

                }

            }else{
                self.performSegue(withIdentifier: "SplashToLogin",sender: self)

            }
        }
    }
    func GCM_UpdateByUserId(GCMId : String) {
           //showLoader(show: true)
           let c : Call = Call<ObjectNull>()
           c.Operation = "CustomSP"
           c.Model = "NOT_NotificationsMobileGCM_UpdateByUserIdAndMobileType"
        c.InputObject = Array (arrayLiteral: ["UserId":AppShared.UserId] , ["GCMId" : GCMId])
           c.IdentityObject = nil
           c.IsSecure = true
           c.Run() {(result: WsResponse<ObjectNull>?, apiError) in
            self.performSegue(withIdentifier: "SplashToMenu",sender: self)
                                      // self.dismiss(animated: true, completion: nil)

           }
       }
}
