//
//  addPhoneViewController.swift
//  Mimwa
//
//  Created by Lenvosoft on 01/10/2020.
//  Copyright © 2020 Lenvosoft. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class addPhoneViewController: UIViewController, CNContactPickerDelegate{
    var delegate : OpenChatListener!
    var numeroADiscar: String = ""
    var userImage: UIImage? = nil
    var nameToSave = ""

    @IBOutlet weak var outletmobilenumber: UITextField!
    @IBAction func ActionAdd(_ sender: Any) {
        let mn : String = outletmobilenumber.text ?? ""
        //var mn : String = "+962795417354"
        haveNumber(phonenumber: mn)
    }
    func haveNumber(phonenumber : String ) {
        var mn = phonenumber
        if(mn.isEmpty){
            return
        }
        if(mn.starts(with: "+"))
        {
            mn = mn.replacingOccurrences(of: "+", with: "")
        }
        mn = mn.replacingOccurrences(of: " ", with: "")
        self.delegate.onHaveNumber(userid: mn)
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)

    }
    @IBAction func ActionOpenContact(_ sender: Any) {

        let peoplePicker = CNContactPickerViewController()
        peoplePicker.delegate = self
        self.present(peoplePicker, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func ActionClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }

    override func didReceiveMemoryWarning() {

        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }

    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {

        picker.dismiss(animated: true, completion: nil)

    }

    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {

        // I only want single selection
        if contacts.count != 1 {

            return

        } else {

            //Dismiss the picker VC
            picker.dismiss(animated: true, completion: nil)

            let contact: CNContact = contacts[0]

            //See if the contact has multiple phone numbers
            if contact.phoneNumbers.count > 1 {

                //If so we need the user to select which phone number we want them to use
                let multiplePhoneNumbersAlert = UIAlertController(title: "Which one?", message: "This contact has multiple phone numbers, which one did you want use?", preferredStyle: UIAlertController.Style.alert)

                //Loop through all the phone numbers that we got back
                for number in contact.phoneNumbers {

                    //Each object in the phone numbers array has a value property that is a CNPhoneNumber object, Make sure we can get that
                    let actualNumber = number.value as CNPhoneNumber

                    //Get the label for the phone number
                    var phoneNumberLabel = number.label

                    //Strip off all the extra crap that comes through in that label
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "_", with: "")
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "$", with: "")
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "!", with: "")
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "<", with: "")
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: ">", with: "")

                    //Create a title for the action for the UIAlertVC that we display to the user to pick phone numbers
                    let actionTitle = phoneNumberLabel! + " - " + actualNumber.stringValue

                    //Create the alert action
                    let numberAction = UIAlertAction(title: actionTitle, style: UIAlertAction.Style.default, handler: { (theAction) -> Void in

                        //See if we can get A frist name
                        if contact.givenName == "" {

                            //If Not check for a last name
                            if contact.familyName == "" {
                                //If no last name set name to Unknown Name
                                self.nameToSave = "Unknown Name"
                            }else{
                                self.nameToSave = contact.familyName
                            }

                        } else {

                            self.nameToSave = contact.givenName

                        }

                        // See if we can get image data
                        if let imageData = contact.imageData {
                            //If so create the image
                            self.userImage = UIImage(data: imageData)!
                        }

                        //Do what you need to do with your new contact information here!
                        //Get the string value of the phone number like this:
                        self.numeroADiscar = actualNumber.stringValue

                    })

                    //Add the action to the AlertController
                    multiplePhoneNumbersAlert.addAction(numberAction)

                }

                //Add a cancel action
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (theAction) -> Void in
                    //Cancel action completion
                })

                //Add the cancel action
                multiplePhoneNumbersAlert.addAction(cancelAction)

                //Present the ALert controller
                self.present(multiplePhoneNumbersAlert, animated: true, completion: nil)

            } else {

                //Make sure we have at least one phone number
                if contact.phoneNumbers.count > 0 {

                    //If so get the CNPhoneNumber object from the first item in the array of phone numbers
                    let actualNumber = (contact.phoneNumbers.first?.value)! as CNPhoneNumber

                    //Get the label of the phone number
                    var phoneNumberLabel = contact.phoneNumbers.first!.label

                    //Strip out the stuff you don't need
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "_", with: "")
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "$", with: "")
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "!", with: "")
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: "<", with: "")
                    phoneNumberLabel = phoneNumberLabel?.replacingOccurrences(of: ">", with: "")

                    //Create an empty string for the contacts name
                    self.nameToSave = ""
                    //See if we can get A frist name
                    if contact.givenName == "" {
                        //If Not check for a last name
                        if contact.familyName == "" {
                            //If no last name set name to Unknown Name
                            self.nameToSave = "Unknown Name"
                        }else{
                            self.nameToSave = contact.familyName
                        }
                    } else {
                        nameToSave = contact.givenName
                    }

                    // See if we can get image data
                    if let imageData = contact.imageData {
                        //If so create the image
                        self.userImage = UIImage(data: imageData)
                    }

                    //Do what you need to do with your new contact information here!
                    //Get the string value of the phone number like this:
                    self.numeroADiscar = actualNumber.stringValue
                    outletmobilenumber.text = self.numeroADiscar

                } else {

                    //If there are no phone numbers associated with the contact I call a custom funciton I wrote that lets me display an alert Controller to the user
                    let alert = UIAlertController(title: "Missing info", message: "You have no phone numbers associated with this contact", preferredStyle: UIAlertController.Style.alert)
                    let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alert.addAction(cancelAction)
                    present(alert, animated: true, completion: nil)

                }
            }
        }

    }

}
