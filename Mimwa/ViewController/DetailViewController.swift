import UIKit

class DetailViewController: UIViewController {

    var user: User? {
        didSet {
            AppShared.currentUser = (user?.getId()) ?? ""
            configureView()
        }
    }
    var list : [Message] = [Message]()
    let data = ["One","Two","Three","Four","Five",]

    @IBOutlet weak var tableview: UITableView!


    override func viewWillDisappear(_ animated: Bool) {
        AppShared.currentUser = ""

    }
    override func viewDidLoad() {
        super.viewDidLoad()


        showNavigationBar(isHidden: false)
        NotificationCenter.default.addObserver(self,selector:#selector(handleObservedNotidaction(sender:)),name:NSNotification.Name(rawValue: "NupdateNotification"),
                                               object: nil)
        tableview.delegate = self
        tableview.dataSource = self
        configureView()
    }
    func Messages_GetConversation_ByRecepientId() {
        let c : Call = Call<Message>()
        c.Operation = "CustomSP"
        c.Model = "Messages_GetConversation_ByRecepientId"
        c.InputObject = Array (arrayLiteral:
                                ["UserId":AppShared.UserId],
                               ["RecepientId": user?.getId() ?? ""] ,
                               ["SystemId" :AppShared.SystemID ],
                               ["ClientId" :AppShared.ClientId ],
                               ["PageNumber" :-1],
                               ["PageSize" :0]
        )
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<Message>?, apiError) in

            if apiError == nil{
                self.list = (result?.Value?.Table!)!
               // print(self.list.count)
               // self.outlistList.reloadData()



            }else{

            }
        }
    }
    @objc func handleObservedNotidaction(sender: NSNotification){

        let noti = NotificationBodyJSON.FromJSON(sender: sender)
        if(noti.isSameUserANDSameSYStem(CurrentUser: (user?.getId())!)){
            //update
        }

    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func configureView() {
        if(user != nil){
            self.title = user?.getName()
            Messages_GetConversation_ByRecepientId()
        }

    }
}
extension DetailViewController : UITableViewDelegate, UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let _ : Int = indexPath.row
      //  var item : Message = list[index]
//        if(index == 3){
//            cell.showOutgoingMessage(color: .red,text: "sadsadasakjhigfdsgfdsigfdufgufgdsfgiufif eif eogfeigiegegehgeoge erg eg erg ege fnslkjng kjd gkl ngsjjkd  gk jgsdas")
//        }else{
         //   cell.showIncomingMessage()
        
        // add border and color
        // cell.backgroundColor = UIColor.clear
        //cell.layer.borderColor = UIColor.gray.cgColor
        //cell.layer.borderWidth = 1
        // cell.layer.cornerRadius = 0
        //cell.clipsToBounds = true
//        let index : Int = indexPath.row
//        var item : User = list[index]
//        //           print(indexPath.row)
//        cell.outletimage.kf.setStringImage(with:item.getAvatar())
//        cell.outletname.text = item.getName()
//        let lastmessage :String = item.LastMessage ?? ""
//        cell.outletlastmessag.text = lastmessage
//        let unread = item.NewMessages ?? 0
//        if(unread <= 0){
//            cell.outletunred.isHidden = true;
//        }else{
//            cell.outletunred.text = "\(unread)"
//        }
//        let lastmessagetime = item.getLastSeenDate()
//        cell.outlettime.text = lastmessagetime




        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        //           let next:PurchasesViewItemViewController = storyboard?.instantiateViewController(withIdentifier: "PurchasesViewItemViewController") as! PurchasesViewItemViewController
        //           next.wishlist = list[indexPath.row]
        //           self.navigationController?.pushViewController(next, animated: true)

    }


}
