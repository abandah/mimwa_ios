import UIKit

class MasterViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchFooter: SearchFooter!
    @IBOutlet var searchFooterBottomConstraint: NSLayoutConstraint!

    var delegate : OpenChatListener!
    var items: [User] = []
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        showNavigationBar(isHidden: false)
        // items = Candy.candies()
        Users_Select_ByPhoneNumber_Filter_Mobile(keyword: "")
        // 1
        searchController.searchResultsUpdater = self
        // 2
        searchController.obscuresBackgroundDuringPresentation = false
        // 3
        searchController.searchBar.placeholder = "Search Contacts"
        // 4
        navigationItem.searchController = searchController
        // 5
        //definesPresentationContext = true

        // searchController.searchBar.scopeButtonTitles = Candy.Category.allCases.map { $0.rawValue }
        // searchController.searchBar.delegate = self

        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification,
                                       object: nil, queue: .main) { (notification) in
            self.handleKeyboard(notification: notification) }
        notificationCenter.addObserver(forName: UIResponder.keyboardWillHideNotification,
                                       object: nil, queue: .main) { (notification) in
            self.handleKeyboard(notification: notification) }
        navigationItem.hidesSearchBarWhenScrolling = false
        self.tableView.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }

    @IBAction func AcionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
     func back() {
        let indexPath = tableView.indexPathForSelectedRow
        let item: User
        item = items[indexPath!.row]
        self.delegate.onHaveNumber(userid:item.getId())
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popToRootViewController(animated: true)

    }
 
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }

    var isFiltering: Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!isSearchBarEmpty || searchBarScopeIsFiltering)
    }

    //    func filterContentForSearchText(_ searchText: String) {
    //        filteredCandies = candies.filter { (candy: Candy) -> Bool in
    //            //  let doesCategoryMatch = category == .all || candy.category == category
    //
    //            return  candy.name.lowercased().contains(searchText.lowercased())
    //
    //        }
    //
    //        tableView.reloadData()
    //    }

    func handleKeyboard(notification: Notification) {
        // 1
        guard notification.name == UIResponder.keyboardWillChangeFrameNotification else {
            searchFooterBottomConstraint.constant = 0
            view.layoutIfNeeded()
            return
        }

        guard
            let info = notification.userInfo,
            let keyboardFrame = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        else {
            return
        }

        // 2
        let keyboardHeight = keyboardFrame.cgRectValue.size.height
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.searchFooterBottomConstraint.constant = keyboardHeight
            self.view.layoutIfNeeded()
        })
    }
    func Users_Select_ByPhoneNumber_Filter_Mobile(keyword :String) {
        //showLoader(show: true)
        let c : Call = Call<User>()
        c.Operation = "CustomSP"
        c.Model = "Users_Select_ByPhoneNumber_Filter_Mobile"
        c.InputObject = Array (arrayLiteral: ["Keyword":keyword],
                               ["FK_SystemID":AppShared.SystemID],
                               ["FK_ClientId":AppShared.ClientId]
        )
        c.IdentityObject = nil
        c.IsSecure = true
        c.Run() {(result: WsResponse<User>?, apiError) in

            if apiError == nil{
                self.items = (result?.Value?.Table)!
                if(self.items.count > 0){
                    self.tableView.reloadData()
                }
            }else{

            }
        }
    }
}

extension MasterViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        searchFooter.setNotFiltering()
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SearchTableViewCell
        var item: User

        item = items[indexPath.row]

        cell.outletname.text = item.getName()
        var _ = cell.outletProfilePic.kf.setStringImage(with:item.getAvatar())

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item: User
        item = items[indexPath.row]
        self.delegate.onHaveNumber(userid:item.getId())
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
}

extension MasterViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        // let category = Candy.Category(rawValue:
        //  searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex])
        Users_Select_ByPhoneNumber_Filter_Mobile(keyword: searchBar.text!)
    }
}


//extension MasterViewController: UISearchBarDelegate {
//  func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
//    let category = Candy.Category(rawValue:
//      searchBar.scopeButtonTitles![selectedScope])
//    filterContentForSearchText(searchBar.text!, category: category)
//  }
//}
